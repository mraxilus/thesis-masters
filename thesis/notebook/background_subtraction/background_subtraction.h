#include <iostream>
#include <opencv2/opencv.hpp>

#include "package_bgs/dp/DPAdaptiveMedianBGS.h"
#include "package_bgs/dp/DPEigenbackgroundBGS.h"
#include "package_bgs/db/IndependentMultimodalBGS.h"
#include "package_bgs/ae/KDE.h"
#include "package_bgs/lb/LBAdaptiveSOM.h"
#include "package_bgs/WeightedMovingVarianceBGS.h"
#include "package_bgs/WeightedMovingMeanBGS.h"
#include "package_bgs/sjn/SJN_MultiCueBGS.h"
#include "package_bgs/KNNBGS.h"
#include "package_bgs/lb/LBFuzzyGaussian.h"
#include "package_bgs/lb/LBMixtureOfGaussians.h"
#include "package_bgs/lb/LBSimpleGaussian.h"
#include "package_bgs/pl/LOBSTER.h"
#include "package_bgs/pl/SuBSENSE.h"
#include "package_bgs/ck/LbpMrf.h"
#include "package_bgs/FrameDifferenceBGS.h"


namespace background_subtraction {
  class Algorithm {
   public:
    IBGS* model;
    Algorithm();
    Algorithm(std::string);
    ~Algorithm();
    cv::Mat process(cv::Mat image);
    int test();
  };
}
