# distutils: language = c++
# distutils: sources = background_subtraction.cpp
# distutils: extra_objects = libbgs.so

from cvt cimport *
from libcpp.string cimport string

cdef extern from 'background_subtraction.h' namespace 'background_subtraction':
    cdef cppclass Algorithm:
        Algorithm() except +
        Algorithm(string) except +
        Mat process(Mat)
        int test()


cdef class PyAlgorithm:
    cdef Algorithm *algorithm
    def __cinit__(self, name=''):
        if name == '':
            self.algorithm = new Algorithm()
        else:
            self.algorithm = new Algorithm(name.encode('UTF-8'))
    def __dealloc__(self):
        del self.algorithm
    def process(self, array):
        cdef Mat matrix = nparray2cvmat(array)
        cdef Mat tmp = self.algorithm.process(matrix)
        return cvmat2nparray(tmp)
    def test(self):
        return self.algorithm.test()

