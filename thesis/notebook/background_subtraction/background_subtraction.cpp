#include "background_subtraction.h"

namespace background_subtraction {
  Algorithm::Algorithm() { 
    model = new DPAdaptiveMedianBGS;
  }

  Algorithm::Algorithm(std::string name) { 
    if(name == "DPAdaptiveMedian") {
      model = new DPAdaptiveMedianBGS;
    } else if(name == "DPEigenbackground") {
      model = new DPEigenbackgroundBGS;
    } else if(name == "IndependentMultimodal") {
      model = new IndependentMultimodalBGS;
    } else if(name == "KDE") {
      model = new KDE;
    } else if(name == "LBAdaptiveSOM") {
      model = new LBAdaptiveSOM;
    } else if(name == "WeightedMovingVariance") {
      model = new WeightedMovingVarianceBGS;
    } else if(name == "WeightedMovingMean") {
      model = new WeightedMovingMeanBGS;
    } else if(name == "SJN_MultiCue") {
      model = new SJN_MultiCueBGS;
    } else if(name == "KNN") {
      model = new KNNBGS;
    } else if(name == "LBFuzzyGaussian") {
      model = new LBFuzzyGaussian;
    } else if(name == "LBMixtureOfGaussians") {
      model = new LBMixtureOfGaussians;
    } else if(name == "LBSimpleGaussian") {
      model = new LBSimpleGaussian;
    } else if(name == "LOBSTER") {
      model = new LOBSTERBGS;
    } else if(name == "SuBSENSE") {
      model = new SuBSENSEBGS;
    } else if(name == "LbpMrf") {
      model = new LbpMrf;
    } else if(name == "FrameDifference") {
      model = new FrameDifferenceBGS;
    } else {
      std::cout << "Fallback..." << std::endl;
      model = new KDE;
    }
  }

  Algorithm::~Algorithm() {
    delete model;
  }

  cv::Mat Algorithm::process(cv::Mat image) {
    cv::Mat mask;
    cv::Mat background;
    model->process(image, mask, background);
    return mask;
  }

  int Algorithm::test() {
    return 42;
  }
}

