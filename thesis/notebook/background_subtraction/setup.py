from distutils.core import setup
from Cython.Build import cythonize

setup(
    name = 'bgslibrary',
    ext_modules = cythonize('*.pyx'),
)

