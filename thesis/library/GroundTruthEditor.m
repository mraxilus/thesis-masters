%%
%% GUI for marking ground truth for object detection
%% 
%%
%% Osian Haines, October 2012 - Cognito
%%

% naming convention (note to self really):
 % lName - local
 % aName - function argument
 % gName - global (to main function)
 % uName - UI object/variable
 % name  - temporary/local
 

function GroundTruthEditor(varargin)
	
	defaultSequencePath = '/Users/csxda/Documents/Documents_local/Hana_DatasetFromSouthampton/VHH09/Rec_03_08_15_12_23_12/img_.Jpeg';
	defaultLabelPath = '/Users/csxda/Documents/Documents_local/output.txt';
	gMonitorId = 0; % 0,1 for left,right monitor
	gDoAutoSave = 1;
	
	
	% Input arguments or settings
		% Count arguments (can be any number from 0, but asks the user for input if none given)
		numArgs = size(varargin,2);
		
		% Argument # 1: input sequence location
		if (numArgs >= 1)
			inputFolderName = varargin{1};
		else
			% otherwise show a popup folder dialog.
			inputFolderName = uigetdir(defaultSequencePath,'Specify image sequence')
			if (inputFolderName == 0)			
				return;
			end
		end
		
		
		% Get label from othe input folder name
		label = '';
		slashLocations = findstr(inputFolderName, '/');
		if (size(slashLocations) > 0)
			lastSlashLocation = slashLocations(end);
			
			if (lastSlashLocation+1 < size(inputFolderName,2))
				label = inputFolderName(lastSlashLocation+1:end);
				% append label with underscore if there is any text in there
				if (size(label,2) > 0)
					label = [label '_'];
				end
			end
		end
		
		% Argument # 2: location for output file
		if (numArgs >= 2)
			outputLocation = varargin{2};
		else
			%outputLocation = inputFolderName;
			outputLocation = uigetdir(defaultLabelPath,'Specify output location')
			if (outputLocation == 0)			
				return;
			end
		end
	
	
		% Choose output file	
		for i = 1:50
			if (~exist(strcat(outputLocation, '/', label, num2str(i), '.txt'), 'file'))
			   break;
			  end;
		end;
		gOutputFileNameToWrite = strcat(outputLocation, '/', label, num2str(i), '.txt');
		disp(['Set output file to ' gOutputFileNameToWrite]);
	
	
	% Read sequence and store names; count them.
		imageFilenames = dir (strcat(inputFolderName, '/*.jpg'));
		gTotalNumImages = size(imageFilenames,1);
		
		% Important!
		if (gTotalNumImages == 0) 
			errordlg('Sorry, no images found in there');
			return
		end
	
	
	
		% Read the third argument now - because we know how many images there are!
	
		if (numArgs >= 3)
			lStartingFrameId = varargin{3};
			if (lStartingFrameId <= 0) 
				disp('You cannot start at a frame before the start of the sequence!');
				lStartingFrameId = 1;
			elseif ((lStartingFrameId > gTotalNumImages)) % ||(strcmp(lStartingFrameId,'end')==1)
				disp('You cannot start at a frame beyond the end of the sequence!');
				lStartingFrameId = gTotalNumImages;
			end
		else
			lStartingFrameId = 1;
		end
		
		
	
	
	
	% variables for all the interface objects - declared here to make them accessible throughout
		uMainFigureHandle = 0;
		uImageAxes = 0;
		%toStartButton = 0;	
		%fastBackwardButton = 0;
		%backwardButton = 0;
		%forwardButton = 0;
		%fastForwardButton = 0;
		%toEndButton = 0;
		uImageIdLabel = 0;
		uCurrentObjectLabel = 0;
		%previousObjectButton = 0;
		%nextObjectButton = 0;
		%clearImageButton = 0;
		%interpolateObjectButton = 0;
		uInterpolationLabel = 0;
		uGotoImageButton = 0;
		uImageTotalLabel = 0;
		uObjectButtonList = [];
		uObjectLabelList = [];
		%uObjectCountTextList = [];
		
	
	% key mapping
		exitKey = 'Q'; % uppercase, so it doesn't get hit accedentally
		forwardKey = 'c';
		backwardKey = 'x';
		fastForwardKey = 'v';
		fastBackwardKey = 'z';
		toStartKey = 'a';
		toEndKey = 'g';
		
		newObjectKey = 'o';
		previousObjectKey = '[';
		nextObjectKey = ']';
		clearImageKey = 'u';
		interpolateKey = 'i';
		
		saveKey = 's';
	
	% UI settings - these values are set in createInterface later
		allowExit = 0; % This basically turns off 'q'. Because it's annoying.
	
		% These get set on creation of UI
		uImageWidth = 0;
		uImageHeight = 0;
		uWindowPosX = 0;
		uWindowPosY = 0;
		uInterfaceWidth = 0;
		uInterfaceHeight = 0;
	
		uGreyBoxColour = [0.4 0.4 0.4];
		uObjectButtonListX = 0;
		uObjectButtonListY = 0;
	
	
	
	% Object creation / selection
	
	gCurrentObjectId = -1;
	gTotalNumObjects = 0;
	gInterpolationId = 0;

	gObjectColours = [];
	gObjectColours(:,1) = [255 255 0];
	gObjectColours(:,2) = [0 255 0]; 
	gObjectColours(:,3) =  [255 0 0];
	gObjectColours(:,4) =  [0 0 255];
	gObjectColours(:,5) =  [255 0 255];
	gObjectColours(:,6) =  [0 255 255];
	gObjectColours(:,7) =  [255 100 0];
	gObjectColours(:,8) =  [100 255 0];
	gObjectColours(:,9) =  [0 255 100];
	gObjectColours(:,10) =  [0 100 255];
	  
	gObjectNames = cell(0);
	
	% For saving objects
	gObjectMarkupList = zeros (0,6); % this is 'gt' in the original version
	
	
	% Start here for reading images! 
	gCurrentImageId = lStartingFrameId;
	
	
	%% ========== ========== ========== ========== ========== ========== ==========
	
	
	%% Set up and start:
	createInterface();	
	showCurrentImage();
	
	
	
	% End of main function	
	% The rest are nested functions
	
	
	% This is what makes everything you can see
	function createInterface() 
	
		
		% ========================================================
		% Interface
		
		% query actual screen size - to display centred
		screenSize = get( 0, 'ScreenSize' );
		screenWidth = screenSize(3);
		screenHeight = screenSize(4);
		
		if (screenWidth > 1600)
			% Assume that means dual monitor - centre to just one!
			screenWidth = screenWidth/2;
		end
		
		
		leftMargin = 10;
		rightMargin = 230;
		bottomMargin = 60;
		topMargin = 70;
		seekButtonWidth = 40;
		
		
		% Read first image - to get size to create interface
		lFirstImage = imread (strcat (inputFolderName, '/', imageFilenames(1).name));
		imageSize = size(lFirstImage);
		uImageWidth = imageSize(2);
		uImageHeight = imageSize(1);
		
		% How big to make the window - dpends n image size and how much extra for buttons is desired
		uInterfaceWidth = uImageWidth + leftMargin*2 + rightMargin;
		uInterfaceHeight = uImageHeight + 10+bottomMargin+topMargin;
		
		windowPosX = screenWidth/2 - uInterfaceWidth/2 + screenWidth*gMonitorId;
		windowPosY = screenHeight/2 - uInterfaceHeight/2;
		
		
		% Where to put the buttons for new objects - obn the right of the image
		uObjectButtonListX = uImageWidth+20;
		uObjectButtonListY = uImageHeight+20;
		
		
	
		%  Create the GUI - this is the figure on which everything else is drawn
		uMainFigureHandle = figure('Visible','off','Position',[windowPosX,windowPosY,uInterfaceWidth,uInterfaceHeight],...
							'KeyPressFcn',@keyPressCallback, 'ButtonDownFcn', @mouseCallback);


		%---------- ---------- ---------- ---------- ---------- ----------
		% This row is the controls for the object selection / creation
		
		objectButtonYposition = bottomMargin + uImageHeight + 40;
		objectButtonX = 0;
		objectButtonGap = 5;
		
		newObjectButton = uicontrol('Style','pushbutton','String',' New Object ', 'Position',[leftMargin + objectButtonX, objectButtonYposition , 100,30],...
					'CallBack',{@createNewObject},'KeyPressFcn',@keyPressCallback,...
					'TooltipString',['New Object (' newObjectKey ')']);
					
		objectButtonX  = objectButtonX + 100 + objectButtonGap;
				

		
		previousObjectButton = uicontrol('Style','pushbutton','String','prev', 'Position',[leftMargin + objectButtonX, objectButtonYposition , 70,30],...
					'CallBack',{@selectPreviousObject},'KeyPressFcn',@keyPressCallback,...
					'TooltipString',['Select previous object (' previousObjectKey ')']);
		objectButtonX  = objectButtonX + 70 + objectButtonGap;
		
		nextObjectButton = uicontrol('Style','pushbutton','String','next', 'Position',[leftMargin + objectButtonX, objectButtonYposition , 70,30],...
					'CallBack',{@selectNextObject},'KeyPressFcn',@keyPressCallback,...
					'TooltipString',['Select next object (' nextObjectKey ')']);
		objectButtonX  = objectButtonX + 70 + objectButtonGap;
					
		clearImageButton = uicontrol('Style','pushbutton','String','Clear image', 'Position',[leftMargin + objectButtonX, objectButtonYposition , 120,30],...
					'CallBack',{@clearImageOfObjects},'KeyPressFcn',@keyPressCallback,...
					'TooltipString',['Clear image (' clearImageKey ')']);
		objectButtonX  = objectButtonX + 120 + objectButtonGap;
					
		clearSequenceButton = uicontrol('Style','pushbutton','String','Clear sequence', 'Position',[leftMargin + objectButtonX, objectButtonYposition , 120,30],...
					'CallBack',{@clearSequenceOfObjects},'KeyPressFcn',@keyPressCallback,...
					'TooltipString',['Clear sequence']); % no hotkey - risky
		objectButtonX  = objectButtonX + 120 + objectButtonGap;
					
			
				
		interpolateButton = uicontrol('Style','pushbutton','String','Interpolate', 'Position',[leftMargin + objectButtonX, objectButtonYposition , 136,30],...
					'CallBack',{@doInterpolation},'KeyPressFcn',@keyPressCallback,...
					'TooltipString',['Interpolate rectangles between current and past frame (' interpolateKey ')']);	
					
		
		uInterpolationLabel = uicontrol('Style','text','string','interpolation', 'Position',[leftMargin + objectButtonX, objectButtonYposition - 35 , 136,30],...
					'BackgroundColor',uGreyBoxColour);
		objectButtonX  = objectButtonX + 140 + objectButtonGap;
		
		
					
		objectButtonX  = objectButtonX + 120 + objectButtonGap;
					
		
		
		%---------- ---------- ---------- ---------- ---------- ----------
		
		uCurrentObjectLabel = uicontrol('Style','text','string','no objects', 'Position',[leftMargin, objectButtonYposition - 35 , 250,30],...
					'BackgroundColor',uGreyBoxColour);
		
		%objectButtonX  = objectButtonX + 80 + objectButtonGap;
		
		
		% Panel for the image
		uImageAxes = axes('Units','Pixels','Position',[leftMargin,bottomMargin,uImageWidth,uImageHeight]); %, 'ButtonDownFcn', @axesMouseCallback); 
		
		%'KeyPressFcn',@keyPressCallback


		%  Interface buttons		
		
		
		
		lSeekButtonX = leftMargin;
		lSeekButtonGap = 0;
		%---------- ---------- ---------- ---------- ---------- ----------
		toStartButton = uicontrol('Style','pushbutton','String',' |< ', 'Position',[lSeekButtonX,10,seekButtonWidth,30],...
					'CallBack',{@skipToStartCallback},'KeyPressFcn',@keyPressCallback,...
					'TooltipString',['To Start (' toStartKey ')']);
		lSeekButtonX = lSeekButtonX + seekButtonWidth + lSeekButtonGap;
		%---------- ---------- ---------- ---------- ---------- ----------		
					
		fastBackwardButton = uicontrol('Style','pushbutton','String',' << ', 'Position',[lSeekButtonX,10,seekButtonWidth,30],...
					'CallBack',{@skipFramesButtonCallback,-10},'KeyPressFcn',@keyPressCallback,...
					'TooltipString',['Rewind (back 10) (' fastBackwardKey ')']);
		lSeekButtonX = lSeekButtonX + seekButtonWidth + lSeekButtonGap;
					
		backwardButton = uicontrol('Style','pushbutton','String',' < ', 'Position',[lSeekButtonX,10,seekButtonWidth,30],...
					'CallBack',{@skipFramesButtonCallback,-1},'KeyPressFcn',@keyPressCallback,...
					'TooltipString',['Backward (' backwardKey ')']);
		lSeekButtonX = lSeekButtonX + seekButtonWidth + lSeekButtonGap;
					
		forwardButton = uicontrol('Style','pushbutton','String',' > ', 'Position',[lSeekButtonX,10,seekButtonWidth,30],...
					'CallBack',{@skipFramesButtonCallback,1},'KeyPressFcn',@keyPressCallback,...
					'TooltipString',['Forward (' forwardKey ')']);
		lSeekButtonX = lSeekButtonX + seekButtonWidth + lSeekButtonGap;
					
		fastForwardButton = uicontrol('Style','pushbutton','String',' >> ', 'Position',[lSeekButtonX,10,seekButtonWidth,30],...
					'CallBack',{@skipFramesButtonCallback,10},'KeyPressFcn',@keyPressCallback,...
					'TooltipString',['Fast forward (skip 10) (' fastForwardKey ')']);
		lSeekButtonX = lSeekButtonX + seekButtonWidth + lSeekButtonGap;
		
		%---------- ---------- ---------- ---------- ---------- ----------
		toEndButton = uicontrol('Style','pushbutton','String',' >| ', 'Position',[lSeekButtonX,10,seekButtonWidth,30],...
					'CallBack',{@skipToEndCallback},'KeyPressFcn',@keyPressCallback,...
					'TooltipString',['To End (' toEndKey ')']);
		lSeekButtonX = lSeekButtonX + seekButtonWidth + lSeekButtonGap;
					
		%---------- ---------- ---------- ---------- ---------- ----------   
		
		uImageIdLabel = uicontrol('Style','edit','string','f#','Position',[lSeekButtonX,10,seekButtonWidth+20,30]);
					%'KeyPressFcn',@imageIdLabelReturnCallback,...); would like to have Return callback, don't know how yet
		lSeekButtonX = lSeekButtonX + seekButtonWidth+20 + lSeekButtonGap;
		
		uGotoImageButton = uicontrol('Style','pushbutton','String','Go', 'Position',[lSeekButtonX,10,seekButtonWidth,30],...
					'CallBack',{@gotoImageLabelCallback},'KeyPressFcn',@keyPressCallback,...
					'TooltipString',['To End (' toEndKey ')']);
		lSeekButtonX = lSeekButtonX + seekButtonWidth + lSeekButtonGap;
		
		uImageTotalLabel  = uicontrol('Style','edit','string','Total images and progress','Position',[lSeekButtonX,10,260,30]);
		lSeekButtonX = lSeekButtonX + 260 + lSeekButtonGap;
		
		
		%---------- ---------- ---------- ---------- ---------- ----------
		
		saveNowButton = uicontrol('Style','pushbutton','String','Save', 'Position',[uImageWidth + leftMargin + 10,10,seekButtonWidth+10,30],...
				'CallBack',{@saveNow},'KeyPressFcn',@keyPressCallback,...
				'TooltipString',['Save to file now (' saveKey ')']);
		%lSeekButtonX = lSeekButtonX + seekButtonWidth + lSeekButtonGap;	
		
		saveAutoCheckbox  = uicontrol('Style','checkbox','String','Auto save', 'Position',[uImageWidth + leftMargin + 10 + seekButtonWidth + lSeekButtonGap+16,10,100,30],...
				'CallBack',{@autoSaveCheckCallback},'KeyPressFcn',@keyPressCallback,...
				'TooltipString',['Auto-save after every action or not']);
		
		set(saveAutoCheckbox,'value',gDoAutoSave);
		
		lSeekButtonX = lSeekButtonX + seekButtonWidth + lSeekButtonGap;	
					
					
		%---------- ---------- ---------- ---------- ---------- ----------
		
		% This is up on the right:
		
		lObjectButtonsLabel = uicontrol('Style','text','String','Objects:',...
					'Position',[uObjectButtonListX,uObjectButtonListY+30 , 100,20],...
					'KeyPressFcn',@keyPressCallback);
					
		lObjectRenamingLabel = uicontrol('Style','text','String','names:',...
					'Position',[uObjectButtonListX + 102,uObjectButtonListY+30 , 100,20],...
					'KeyPressFcn',@keyPressCallback);
					
		
		
		%---------- ---------- ---------- ---------- ---------- ----------   
		
	
		set(uMainFigureHandle,'Visible','on');
	end
	
		
		
	% This is important - shows the current image on the axes, but also updates various counters and UI displays	
	function showCurrentImage()
		disp(['Showing image # ' num2str(gCurrentImageId)]);
		% the current image is only local to this function, not needed elsewhere
		lCurrentImage = imread (strcat (inputFolderName, '/', imageFilenames(gCurrentImageId).name));
		
		% Create an image on the axes - so don't need to do an imshow.
		imageHandle = image(lCurrentImage,'Parent',uImageAxes); %Also with: h = imagesc(IM,'Parent',gca);
		set(imageHandle,'ButtonDownFcn',{@axesMouseCallback});
		
		
		set(uImageIdLabel,'string',num2str(gCurrentImageId));
		set(gca,'XTick',[])
		set(gca,'YTick',[])
		
		% Find whether there are any obbjects recorded for this frame:
		recordIdsForFrame = find(gObjectMarkupList(:,1) == gCurrentImageId);
		
		numObjectsThisFrame = size(recordIdsForFrame,1);
		% If there are any - loop through and draw with appropriate colours
		if (numObjectsThisFrame > 0)
			hold on;
			for (i = 1:numObjectsThisFrame)
				recordId = recordIdsForFrame(i);
				
				imageRecord = gObjectMarkupList(recordId,:);
				objectId = imageRecord(2);
				minX = imageRecord(3);
				minY = imageRecord(4);
				maxX = imageRecord(5);
				maxY = imageRecord(6);
				thisObjectCol = gObjectColours(:,objectId)/255.0;
				plot([minX minX maxX maxX minX],[minY maxY maxY minY minY],'Color',thisObjectCol,'LineWidth',3);
				text(minX+4,maxY-10,['' num2str(objectId) ' - ' gObjectNames{objectId}],'Color',thisObjectCol*0.7) 
				
				
				
			end
		end
		
		% check for the possibility of interpolation, to show on the viewing label
		checkInterpolation();
		updateObjectCounts();
		
		
		allImagesLabelled = gObjectMarkupList(:,1);
		uniqueImagesLabelled = unique(allImagesLabelled);
		%minLabelled = min(uniqueImagesLabelled)
		%maxLabelled = max(uniqueImagesLabelled)
		numLabelled = size(uniqueImagesLabelled,1);
		%idsOfLabelled
		%totalImagesLabelled
		message = ['Labelled ' num2str(numLabelled) ' of ' num2str(gTotalNumImages) ' images'];
		%if (numLabelled > 1)
			%message = [message '(between ' num2str(minLabelled) ' and ' num2str(maxLabelled)];
		%end
		set(uImageTotalLabel,'string',message);
		
	end
	
	
%% === Video navigation ==

	
	function skipToStartCallback(object, data)
		gCurrentImageId = 1;
		showCurrentImage();
	end
	function skipToEndCallback(object, data)
		gCurrentImageId = gTotalNumImages;
		showCurrentImage();
	end
	
	function skipFramesButtonCallback(object, data, number)
		skipFrames(number);
	end
	
	
	%% This gets the text from the current-image text box and skips straight to it
	%% If less than 0, or not a number, ignores it, and resets text to current frame
	%% If beyond end, skips to end
	function gotoImageLabelCallback(object, data)
		lLabelString = get(uImageIdLabel,'string')
		[lNewId,status] = str2num(lLabelString)
		if (status == 0)
			disp('Invalid input!');
		elseif (lNewId <= 0)
			disp('Invalid id');
			% reset the string to the current image:
			set(uImageIdLabel,'string',num2str(gCurrentImageId))
		elseif (lNewId > gTotalNumImages)
			disp('Beyond end!');
			gCurrentImageId = gTotalNumImages
			showCurrentImage();
		else
			gCurrentImageId = lNewId
			showCurrentImage();
		end
	end
	
	
		
	function skipFrames(numToSkip)
		% Yes it's possible to skip zero frames if you want to just reload (unused)
		%if (numToSkip == 0)
			%disp('Zero makes no sense');
			%return;
		%end
		if ( ((gCurrentImageId + numToSkip)> 0) && ((gCurrentImageId + numToSkip)< gTotalNumImages))
			gCurrentImageId = gCurrentImageId + numToSkip;
		else
			if (numToSkip < 0)
				disp('Start of video!');
				gCurrentImageId = 1
			else
				disp('End of video!');
				gCurrentImageId = gTotalNumImages
			end
		end
		showCurrentImage();
	end
	
%% === Input / output ==
	
	function saveNow(uobject,udata) 
		disp('Writing to file...');
		dlmwrite (gOutputFileNameToWrite, gObjectMarkupList);
	end
	
	function autoSaveCheckCallback(uobject,udata) 
		gDoAutoSave = get(uobject,'value');
		if (gDoAutoSave == 1)
			disp('Auto-save is on');
			saveNow(0,0);
		else
			disp('Auto-save is off');
		end
	end
	
%% === Drawing objects ==

	function [MinPoint,MaxPoint] = getDrawnBox() 
	
		point1 = get(gca,'CurrentPoint');    % button down detected
		boxCorners = rbbox;
		point2 = get(gca,'CurrentPoint');    % button up detected

		point1 = point1(1,1:2);              % extract x and y
		point2 = point2(1,1:2);
		
		minX = min(point1(1),point2(1));
		minY = min(point1(2),point2(2));
		maxX = max(point1(1),point2(1));
		maxY = max(point1(2),point2(2));
		
		MinPoint = [minX,minY];
		MaxPoint = [maxX,maxY];
	end
	
	
	%% Function called when clicking/dragging on the image
	function axesMouseCallback(object, data) 
	
		if (gCurrentObjectId < 0)
			errordlg('No active object! Create or select one');
			return;
		end
		
				
		[MinPoint,MaxPoint] = getDrawnBox();
		
		minX = MinPoint(1);
		minY = MinPoint(2);
		maxX = MaxPoint(1);
		maxY = MaxPoint(2);		
		
		% Don't redraw yet - that's done by re-showing the image
	
		isOk = recordObjectBoundary(gCurrentImageId, gCurrentObjectId, minX,minY,maxX,maxY);
		if (isOk)
			showCurrentImage();
		else
			disp('Error');
		end
		
	end
	
	% Record the boundary given, for the specified image and object, to the list (return success)
	function [isOk] = recordObjectBoundary(aImageId, aObjectId, aMinX,aMinY,aMaxX,aMaxY)
	
		% Check ranges!
		if ((aMinX <= 0)||(aMinY <= 0)||(aMaxX > uImageWidth)||(aMaxY > uImageHeight))
			disp('Outside of range!');
			isOk = 0;
			return;
		end
		
		if ((aMinX == aMaxX)||(aMinY == aMaxY))
			disp('Zero size!');
			isOk = 0;
			return;
		end
		
		% Overwite existing object record if this object ID has already been used in this image
		
		%find(gObjectMarkupList(:,1) == imageId)	
		
		numRecords = size(gObjectMarkupList,1);
		
		idOfExisting = find((gObjectMarkupList(:,1) == aImageId) & (gObjectMarkupList(:,2) == aObjectId));
		if ((numRecords > 0) & ( size(idOfExisting) > 0))
			disp(['Object ' num2str(aObjectId) ' already exists on image ' num2str(aImageId) ' - will overwrite']);
			
			idToWrite = idOfExisting;
		else
			idToWrite = numRecords+1;
		end
		
		
		% set all columns of this record
		gObjectMarkupList(idToWrite,:) = [aImageId aObjectId aMinX aMinY aMaxX aMaxY];
		
		
		if (gDoAutoSave == 1)	
			saveNow(0,0);
		end
		
		%updateObjectCounts();
		% refresh image - which will lookup and draw the objects
		isOk = 1;
	end
	
	% Each object button displays a count of how many instances there are of that object
	% This counts up all objects and updates (not an efficient way of dong it!)
	function updateObjectCounts()
		for (i=1:gTotalNumObjects)
			objectId = i;
			%objectCountLabel = uObjectCountTextList(objectId)
			allRecIds = find(gObjectMarkupList(:,2) == objectId);
			objectTotal = size(allRecIds,1);
			%set(objectCountLabel,'string',['Count: ' num2str(objectTotal)])
			lObjectButton = uObjectButtonList(objectId);
			set(lObjectButton,'string',['Object ' num2str(objectId) ' (' num2str(objectTotal) ')'])
		end
	end

%% === Managing objects ==
	function createNewObject(uiobject,uidata)
		lNewObjectId = gTotalNumObjects+1;
		gCurrentObjectId = lNewObjectId;
		gTotalNumObjects = gTotalNumObjects + 1;
		
		
		gObjectNames{lNewObjectId} = 'unnamed';
		
		if (lNewObjectId > size(gObjectColours,2))
			disp('Not enough colours! Making new ones...');
			gObjectColours(:,lNewObjectId) = rand(3,1)*255;
		end
		
		setCurrentObject(lNewObjectId);
		
		lW = 100;
		lH = 24;
		
		
		% Create buttons for new object, add to imterface
		% Also store buttons/labels in list, for later access
		lNewObjectButton = uicontrol('Style','pushbutton','String',['Object ' num2str(lNewObjectId) ' (0)'],...
					'BackgroundColor',gObjectColours(:,lNewObjectId)/255.0,...
					'Position',[uObjectButtonListX,uObjectButtonListY , lW,lH],...
					'CallBack',{@selectObjectCallback,lNewObjectId},'KeyPressFcn',@keyPressCallback,...
					'TooltipString',['Click to select']);
		uObjectButtonList(lNewObjectId) = lNewObjectButton;
		
		
		
		
		lNewObjectLabel = uicontrol('Style','edit','String',gObjectNames{lNewObjectId},...
					'Position',[uObjectButtonListX + lW+ 2,uObjectButtonListY , lW,lH]);
		uObjectLabelList(lNewObjectId) = lNewObjectLabel;					
					%'KeyReleaseFcn',{@renameObjectFromLabel,lNewObjectId},...
		
		lNewObjectButton = uicontrol('Style','pushbutton','String','OK',...
					'BackgroundColor',gObjectColours(:,lNewObjectId)/255.0,...
					'Position',[uObjectButtonListX + lW+ 2 + lW+ 2,uObjectButtonListY , 20,lH],...
					'CallBack',{@renameObjectFromLabelButton,lNewObjectId},'KeyPressFcn',@keyPressCallback,...
					'TooltipString',['Click to set name']);
		
		%uImageIdLabel = uicontrol('Style','edit','string','f#','Position',[lSeekButtonX,10,seekButtonWidth+20,30]);

		%lNewObjectCountText = uicontrol('Style','text','String','Count',...
					%'Position',[gObjectButtonListX + lW+ 2 + lW+ 2 + 22,gObjectButtonListY , lW,lH]);
		%uObjectCountTextList(lNewObjectId) = lNewObjectCountText;
								

		% Move down the page for the next one
		uObjectButtonListY = uObjectButtonListY - lH-1;
			
		
		
		
		
	end
	
	
	% Not used, but want to update the object name as it is typed
	function renameObjectFromLabel(uiobject,uidata,aObjectId)
		disp('Callback');
		%labelText = get(uiobject,'string');
		%disp(['The label says ' labelText]);
		%gObjectNames{aObjectId} = labelText;
		%set(uCurrentObjectLabel,'string',['Current object: ' num2str(aObjectId) ' : ' gObjectNames{aObjectId}]);
	end
	function renameObjectFromLabelButton(uiobject,uidata,aObjectId)
		
		lObjectLabel = uObjectLabelList(aObjectId);
		lNewName = get(lObjectLabel,'String');
		
		disp(['Renaming object ' num2str(aObjectId) ' to ' lNewName]);
		
		gObjectNames{aObjectId} = lNewName;
		
		set(uCurrentObjectLabel,'string',['Current object: ' num2str(aObjectId) ' : ' gObjectNames{aObjectId}]);
		% redraw - so that any current objects are renamed in view
		showCurrentImage();
	end
	
	
	% Get rid of all objects in the current image, 
	function clearImageOfObjects(uiobject,uidata)
		% Find the IDs of all the records in this frame and remove them
		
		disp('before');
		recordIdsForFrame = find(gObjectMarkupList(:,1) == gCurrentImageId)		
		%lNumObjectsCurrentFrame = size(recordIdsForFrame,1)
		
		% erase rows!
		gObjectMarkupList(recordIdsForFrame,:) = [];
	
		if (gDoAutoSave == 1)	
			saveNow(0,0);
		end
		
		showCurrentImage();
		
	end
	
	% Delete everything! This is quite drastic so there is a check first
	function clearSequenceOfObjects(uiobject,data)
	
		%doRemoval = inputdlg('Warning - this will remove ALL objects marked in ALL images - continue?')
		doClearQuestion = questdlg('Warning - this will remove ALL objects marked in ALL images - continue?',...
							'Clear all?','OK','Cancel','Cancel');
							
		%if (doClearQuestion == 'OK')
		if (strcmp(doClearQuestion,'OK'))
			% Just reset all...
			disp('Clear ALL');
			gObjectMarkupList = zeros (0,6);
			if (gDoAutoSave == 1)	
				saveNow(0,0);
			end
				
			showCurrentImage();
		end
	end



%% === Viewing objects ==	
	function setCurrentObject(objId)
		gCurrentObjectId = objId;
		if ((objId < 1)||(objId > gTotalNumObjects)) 
			disp(['Object ' num2str(objId) ' not valid']);
		end
		set(uCurrentObjectLabel,'string',['Current object: ' num2str(gCurrentObjectId) ' : ' gObjectNames{gCurrentObjectId}]);
		lNewObjectCol = gObjectColours(:,gCurrentObjectId)/255.0;
		set(uCurrentObjectLabel,'BackgroundColor',lNewObjectCol);
		
		%if (gCurrentObjectId <= size(uObjectButtonList,1))
			%objectButtonPtr = uObjectButtonList(gCurrentObjectId)
			%set(objectButtonPtr,'string','*****');
		%end
		
		% Re-check for interpolating this object
		checkInterpolation();
	end		

	function selectObjectCallback(uiobject,uidata,objectId)
		setCurrentObject(objectId);
	end
	
	function selectPreviousObject(uiobject,uidata)
		if (gTotalNumObjects == 0) 
			return;
		
		else 
			if (gCurrentObjectId == -1)
				setCurrentObject(gTotalNumObjects);
			elseif (gCurrentObjectId > 1)
				setCurrentObject(gCurrentObjectId -1);
			else
				disp('no previous object!');
			end
		end
	end
	
	function selectNextObject(uiobject,uidata)
		if (gTotalNumObjects == 0) 
			return;
		
		else 
			if (gCurrentObjectId == -1)
				setCurrentObject(1);
			elseif (gCurrentObjectId < gTotalNumObjects)
				setCurrentObject(gCurrentObjectId +1);
			else
				disp('no next object!');
			end
		end
	end
	

	
%% === Interpolation ==
	
	
	%% This is to see if the current image (if there is an object in it) can be interoplated back to anywhere
	%% Shows the information in a label, and stores it in case needed
	function checkInterpolation()
	
		gInterpolationId = 0;
		if (gCurrentObjectId > 0)
			% want to know if we can interpolate
			% for the current object - find all frames it is in
			
			% get the record IDs that contain this object
			recordIdsForObject = find(gObjectMarkupList(:,2) == gCurrentObjectId);
			% for all those record IDs - get their frames from the record
			frameIdsForObject = gObjectMarkupList(recordIdsForObject,1);
			% is the object in THIS frame? otherwise obviously can't interpolate...
			idHasCurrent = find(frameIdsForObject == gCurrentImageId);
			if ( size(idHasCurrent) > 0 )
				% want to know of any frames the object is is, that is less than (before) the current frame
				% what this gives you is IDs in the frameIdsForObject list
				idsLessThanNow = find(frameIdsForObject < gCurrentImageId);
				% from the IDs in that list - find what their frame IDs really are
				frameIdsForObjectInPast = frameIdsForObject(idsLessThanNow);
				
				%sizeOne = size(frameIdsForObjectInPast,1);
				% if there is at least one in the last....
				if (size(frameIdsForObjectInPast,1) > 0)
					% want the most recent one - which actually is NOT always at the end of thelist, because it is not sorted! so get the maximum
					lastPastId = max(frameIdsForObjectInPast); % frameIdsForObjectInPast(end);
					if (lastPastId ~= gCurrentImageId-1)
						%disp(['IP: Shold be able to interpolate back to ' num2str(lastPastId)]);
						gInterpolationId = lastPastId;
					else
						%disp('IP: No, because that would be the last frame');
					end
				else
					%disp('IP: There are no frames in the past for this object');
				end
			else
				%disp('IP: Current object not in frame');
			end
		else
			%disp('IP: No current object');
		end
		
		% update the UI as appropriate - to show the user if they can interpolate and where to
		interpolationColour = [0 0 0];
		if (gInterpolationId > 0)
			set(uInterpolationLabel,'string',['Iterpolate to ' num2str(gInterpolationId)]);
			interpolationColour = [0.2 0.9 0.2];
		else
			set(uInterpolationLabel,'string',['Iterpolation not available']);
			interpolationColour = uGreyBoxColour;
		end
		set(uInterpolationLabel,'BackgroundColor',interpolationColour);
	end
	
	
	%% This does the interpolation between a current image and whatever previous one is possible
	%% Uses the stored interpolation ID found (for the current image) by checkInterpolation()
	function doInterpolation(uiobject,event) 
		if (gInterpolationId > 0)
		
		
			% Find there the current and interpolatable frames are in the markup list
			recordIdOfStart = find( (gObjectMarkupList(:,1) == gInterpolationId) & (gObjectMarkupList(:,2) == gCurrentObjectId));
			recordIdOfEnd = find( (gObjectMarkupList(:,1) == gCurrentImageId) & (gObjectMarkupList(:,2) == gCurrentObjectId));
			
			%so we can get the rectangles for where to start and end
			startRectangle = [ gObjectMarkupList(recordIdOfStart,3) gObjectMarkupList(recordIdOfStart,4) gObjectMarkupList(recordIdOfStart,5) gObjectMarkupList(recordIdOfStart,6)];
			endRectangle = [ gObjectMarkupList(recordIdOfEnd,3) gObjectMarkupList(recordIdOfEnd,4) gObjectMarkupList(recordIdOfEnd,5) gObjectMarkupList(recordIdOfEnd,6)];
			
			framesInBetween = (gInterpolationId+1):(gCurrentImageId-1);
			numFramesToInterpolate = gCurrentImageId-gInterpolationId-1;
			
			% First, check that the object we are interpolating never appears in between the start and end frames
			% It shouldn't, because of the way the interpolation image is calculated, but to be safe...
			for (i = [framesInBetween])
				%check if there is any of the current object there already
				currObjInFrame = find( (gObjectMarkupList(:,1) == i) & (gObjectMarkupList(:,2) == gCurrentObjectId));
				if (size(currObjInFrame,1) > 0)
					disp(['Cannot interpolate, because found an instance of object ' num2str(gCurrentObjectId) ' in between frames ' num2str(recordIdOfStart) ' and ' num2str(recordIdOfEnd) ' at ' num2str(i)]);
					return;
				end
			end
			
			% Do interpolation! do this independently for each corner, linearly interpolate
			interpCount = 1;
			disp(['Interpolating along frames ' num2str(framesInBetween)]);
			for (i = [framesInBetween])
				
				interp = interpCount / (numFramesToInterpolate+1); %add 1 so it doesn't get to end (nor start at 0)
				numRecords = size(gObjectMarkupList,1);				
				idToWrite = numRecords+1;	
						
				%disp(['At frame ' num2str(i) ' will write record ' num2str(idToWrite) ' at interp = ' num2str(interp)]);
				
										
				
				sminX = startRectangle(1);
				sminY = startRectangle(2);
				smaxX = startRectangle(3);
				smaxY = startRectangle(4);							
				
				eminX = endRectangle(1);
				eminY = endRectangle(2);
				emaxX = endRectangle(3);
				emaxY = endRectangle(4);
				
				%start with lop interp, i.e. at beginning
				iminX = sminX*(1-interp) + eminX*interp;
				iminY = sminY*(1-interp) + eminY*interp;
				imaxX = smaxX*(1-interp) + emaxX*interp;
				imaxY = smaxY*(1-interp) + emaxY*interp;
				
				% set all columns of this record
				gObjectMarkupList(idToWrite,:) = [i gCurrentObjectId iminX iminY imaxX imaxY];
				
				interpCount = interpCount + 1;
			end
			
			disp(['Have written ' num2str(interpCount-1) ' frames of the desired, ' num2str(numFramesToInterpolate) ' bweeen ' num2str(gInterpolationId) ' and ' num2str(gCurrentImageId)]);
			
			
			showCurrentImage();
			if (gDoAutoSave == 1)	
				saveNow(0,0);
			end
			
			% Reset this here, after setting from image
			set(uInterpolationLabel,'string',['Iterpolation done!']);
			set(uInterpolationLabel,'BackgroundColor',[0.2 0.5 1]);
		
		else
			disp('Interpolation not possible');
			set(uInterpolationLabel,'string',['Iterpolation fail!']);
			set(uInterpolationLabel,'BackgroundColor',[0.7 0 0]);
		end
	end



%% === General event handling ==



	function keyPressCallback(object, data) 
		char = data.Character;
		%disp(['Key press callback char = ' char]);
		
		if ((char == exitKey))
			skipFrames(1);
			close(uMainFigureHandle);
			
			
		elseif (char == fastBackwardKey)
			skipFrames(-10);
		elseif (char == backwardKey)
			skipFrames(-1);
		elseif (char == forwardKey)
			skipFrames(1);
		elseif (char == fastForwardKey)
			skipFrames(10);
		% Note - this reuses callback functions, just without giving the object,data params.
		elseif (char == toStartKey)
			skipToStartCallback(0,0);
		elseif (char == toEndKey)
			skipToEndCallback(0,0);
			
		elseif (char == newObjectKey)
			createNewObject(0,0);
		elseif (char == previousObjectKey)
			selectPreviousObject(0,0);
		elseif (char == nextObjectKey)
			selectNextObject(0,0);
		elseif (char == clearImageKey)
			clearImageOfObjects(0,0);
		elseif (char == interpolateKey)
			doInterpolation(0,0);
		elseif (char == saveKey)
			saveNow(0,0);
		
			
				
		
		else
			disp(['Unknown key ' char]);
		end
		
	end
	
	
	function mouseCallback(object, data) 	
		%disp('Mouse press callback');
	end
	
end % end of whole function (all others are nested)
