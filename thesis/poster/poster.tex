% Set poster style, orientation, and size.
\documentclass[a1, portrait, 25pt]{sciposter}

% Setup fonts.
\usepackage[default,osfigures,scale=0.95]{opensans}
\usepackage[T1]{fontenc}

% Setup section background color.
\usepackage{color}
\definecolor{SectionCol}{RGB}{171,31,45}
\definecolor{BoxCol}{RGB}{190,185,166}
\definecolor{BoldCol}{RGB}{0,47,95}
\definecolor{GreenCol}{RGB}{2,71,49}
\DeclareTextFontCommand{\textbf}{\bfseries\color{BoldCol}}
\usepackage{caption}
\DeclareCaptionFont{GreenCol}{\color{GreenCol}}
\captionsetup{textfont={bf,small,GreenCol}}

% Set title and author information.
\renewcommand{\titlesize}{\LARGE}
\renewcommand{\authorsize}{\Large}
\renewcommand{\instsize}{\Large}
\renewcommand{\sectionsize}{\Large}
\leftlogo[2.8]{image/logo/combined.png}
\title{\vspace{0.5cm}\color{SectionCol}Tracking Feet Positions from a Single Camera}
\author{\vspace{0.2cm}\color{BoldCol}Sinclair-Emmanuel M. Smith}
\institute{\hspace{-6.7cm}\color{GreenCol}Department of Computer Science \hfill Supervised by Dima Damen\vspace{-2cm}}
\norightlogo{}

% Split content into columns and remove separator.
\usepackage{multicol}
\setlength{\columnseprule}{0pt}

% Allow for clearer identification of paragraphs.
\usepackage{parskip}

% Setup bibliography management.
\usepackage[backend=biber, style=ieee]{biblatex}
\addbibresource{bibliography.bib}

% Allow for the importation of images.
\usepackage{graphicx}
\graphicspath{{image/}}

% Allow for placeholder content.
\usepackage{todonotes}
\usepackage{mwe}

% Allow for sub-figures within figures.
\usepackage{subfig}

% Increase equation size.
\usepackage{amsmath}
\newcommand*{\Scale}[2][4]{\scalebox{#1}{\ensuremath{#2}}}%



\begin{document}

% Setup title with right justification.
\begingroup
\let\center\flushright{}
\let\endcenter\endflushright{}
\maketitle
\endgroup


\section*{Overview}
This project aimed to investigate whether, using current methods in computer vision, we can track the feet positions of human silhouettes.
The dataset used by this project has a high-angle camera perspective which is almost parallel to the direction of travel, whereas existing approaches tend to use a level angled perspective perpendicular to the direction of travel.~\cite{doughty2015automatic}
It uses existing methods for silhouette identification and a heuristic approach to object tracking in the presence of specular reflections caused by inconsistent lighting. 


\begin{multicols}{2}
\section{Preprocessing}
The video used was taken by a GoPro.
The camera captured noise which affects the effectiveness of later stages, so it was removed.

\begin{figure}[!ht]
	\captionsetup{type=figure}
	\hspace{0.05\textwidth}
	\subfloat[Original.]{%
		\includegraphics[width=0.4\textwidth]{original}
	}
	\hspace{0.05\textwidth}
	\subfloat[After noise removal.]{%
		\includegraphics[width=0.4\textwidth]{after_noise_removal}
	}
    \caption{The original and resulting image produced by the colored Non-local Means Denoising implemented by the OpenCV library.~\cite{bradski2000opencv}}
\end{figure}

\section{Background Subtraction}
Next we use a commonly used technique to separate the foreground, such as our moving person, from the background scene.

\begin{figure}[!ht]
	\captionsetup{type=figure}
	\subfloat[KDE.]{%
		\includegraphics[width=0.33\textwidth]{kde}
	}
	\subfloat[Mixture of Gaussians.]{%
		\includegraphics[width=0.33\textwidth]{mixture_of_gaussians}
	}
	\subfloat[Fuzzy Gaussian.]{%
		\includegraphics[width=0.33\textwidth]{fuzzy_gaussian}
	}
    \caption{The foreground masks produced by the three different background subtraction techniques used from the BGSLibrary.~\cite{sobral2013bgslibrary}}
\end{figure}
\vspace{-5mm}

The masks are then combined in a manner similar to the following, with the addition of morphological filters to remove shadows:

\begin{equation}
\Scale[2]{\sum_{i=0}^{n}\frac{1}{n}* I_i}
\end{equation}

Where \Scale[1.4]{i} is the index, \Scale[1.4]{n} is the total number, and \Scale[1.2]{I} is the mask.

\begin{figure}[!ht]
	\captionsetup{type=figure}
	\hspace{0.05\textwidth}
	\subfloat[Combined.]{%
		\includegraphics[width=0.4\textwidth]{combined}
	}
	\hspace{0.05\textwidth}
	\subfloat[Thresholded.]{%
		\includegraphics[width=0.4\textwidth]{thresholded}
	}
    \caption{The image generated from combining the foreground masks produced by the three methods, both before and after thresholding.}
\end{figure}



\columnbreak{}
\section{Tracking}
The main component of this project is the detection and the tracking of individual human feet over time.
This is accomplished using a combination of heuristic methods and regression analysis.

\begin{figure}[!ht]
	\captionsetup{type=figure}
	\hspace{0.05\textwidth}
	\subfloat[Contour.]{%
		\includegraphics[width=0.4\textwidth]{contour}
	}
	\hspace{0.05\textwidth}
	\subfloat[Detections.]{%
		\includegraphics[width=0.4\textwidth]{detections}
	}
    \caption{The contour produced by the silhouette as well as the resulting foot detections, which take into account size proportions (where yellow is the detection for the left foot, and green is for the right).}
\end{figure}

Left and right are differentiated by comparing the mean of a historical window of foot locations with that of the current frame.


\section{Results}
The results are measured by taking the intersection of the detected and ground truth bounding regions over the union of the two.
This is known as the \textbf{Intersection-over-Union (IU)} score.

\vspace{-5mm}
\begin{figure}[!ht]
	\captionsetup{type=figure}
	\hspace{0.05\textwidth}
	\subfloat[Ground truth comparison.]{%
        \includegraphics[width=0.45\textwidth]{comparison}
	}
	\subfloat[IU score over time.]{%
        \includegraphics[width=0.5\textwidth]{iu_score_total}
	} \\
	\subfloat[Left distribution.]{%
		\includegraphics[width=0.5\textwidth]{iu_score_left}
	}
	\subfloat[Right distribution.]{%
		\includegraphics[width=0.5\textwidth]{iu_score_right}
	}
    \caption{A visual comparison between the predicted bounding region and the ground truth label (where white are the labels, red is for the left intersection, and blue is for the right), the windowed IU score for the whole image sequence, and the score distributions for both feet.}
\end{figure}

\end{multicols}

% Setup references.
\nocite{*}
\printbibliography{}

\end{document}

