\chapter{Application Code}
\begin{listing}[H]
\begin{minted}[frame=lines, framerule=\heavyrulewidth, framesep=6pt, linenos, fontsize=\scriptsize]{python}
# List of channels containing pixel values.
Image = numpy.array

# BGR, RGB, etc.
Color = Tuple[int, int, int]

# (x, y, w, h).
Rectangle = Tuple[int, int, int, int]

# Where key is 'left' or 'right'.
# Where key is Label frame index.
Label = Dict[str, Rectangle]
Labels = Dict[int, Label]

# List of points (x, y) along an image contour.
Contour = numpy.array

# Values for left and right foot.
Foot = Rectangle
Feet = Tuple[Rectangle, Rectangle]

# Both feet with detection validation or coresponding image index.
Detection = Tuple[Foot, Foot, Union[bool, int]]

# Name, object, and weight.
Algorithm = Tuple[str, bgs.PyAlgorithm, float]
\end{minted}
\caption[Definitions of the Data Types]{%
	The definitions of the data types used throughout the system.
    The \mintinline{python}{bgs.PyAlgorithm} is a Python wrapper created to interface with the BGSLibrary.
    This was attained using the Cython C-extensions.
}
\label{lst:data_types}
\end{listing}

\begin{listing}[H]
\begin{minted}[frame=lines, framerule=\heavyrulewidth, framesep=6pt, linenos, fontsize=\scriptsize]{python}
def load_image(path: str, cached: bool=True) -> Image:
    """Load a color image from a specified path."""
    # Determine cached image path.
    path_split = path.split(sep='/')
    filename = path_split[-1]
    directory = path_split[-2]
    directory_cached = 'image/' + directory
    path_cached = directory_cached + '/' + filename

    # Load cached image.
    if cached and os.path.isfile(path_cached):
        return cv2.imread(path_cached, cv2.IMREAD_COLOR)

    # Create a cached version of source image.
    image = cv2.imread(path, cv2.IMREAD_COLOR)
    image = cv2.fastNlMeansDenoisingColored(image)
    if not os.path.exists(directory_cached):
        os.makedirs(directory_cached)
    cv2.imwrite(path_cached, image)

    return image
\end{minted}
\caption[Implementation of the Pre-Processing Steps]{%
    The implementation of the pre-processing steps taken by the system.
    This includes the detection and creation of the cached image.
}
\label{lst:preprocessing}
\end{listing}

\begin{listing}[H]
\begin{minted}[frame=lines, framerule=\heavyrulewidth, framesep=6pt, linenos, fontsize=\scriptsize]{python}
def detect_feet_positions(image: Image, algorithms: List[Algorithm],
			  history: List[Detection], back_facing: bool,
			  **parameters: Dict[str, Any]) -> Detection:
    """Detect the positions of feet within an image and provide a confidence."""
    # Seperate the image forground from the background.
    mask = perform_background_subtraction(image, algorithms)

    # Generate silhouette contour.
    contour = find_contour(mask)
    bounding_box = cv2.boundingRect(contour)
    if contour is None or not bounding_box:
        return None, None, False

    # Determine location and size of feet in the image.
    left, right = locate_feet(bounding_box, contour, back_facing)
    image_foot = image.copy()
    if not left or not right:
        return None, None, False

    # Assess if located feet fit with recent trajectories.
    left_result, right_result = are_inliers(left, right, history)
    if not left_result or not right_result:
        return left, right, False

    return left, right, left_result and right_result
\end{minted}
\caption[High Level Abstraction of the System]{%
	The high level abstraction encompassing how the system performs detections.
	This function is called for every frame in the sequence.
}
\label{lst:detect_feet_positions}
\end{listing}

\begin{listing}[H]
\begin{minted}[frame=lines, framerule=\heavyrulewidth, framesep=6pt, linenos, fontsize=\scriptsize]{python}
def perform_background_subtraction(image: Image, algorithms: List[Algorithm],
				   threshold: int=175) -> Image:
    """Perform background subtraction on an image using multiple algorithms."""
    # Generate foreground masks from each algorithm.
    masks = []
    for _, model, _ in algorithms:
        mask = model.process(image)
        masks.append(mask)

    # Combine masks by weighted addition.
    mask_combined = numpy.zeros(image.shape[:2], dtype=numpy.uint8)
    weights = [algorithm[2] for algorithm in algorithms]
    for i in range(2):
        for mask, weight in zip(masks, weights):
            if mask.shape == (0, 0):
                continue

            # Handle algorithms which produce color masks.
            if len(mask.shape) == 3:
                mask = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY)

            mask = cv2.GaussianBlur(mask, (9, 9), 0)
            mask_combined = cv2.addWeighted(mask_combined, 1, mask, weight, 0)
        if i == 0:  # Use first pass to remove shadow.
            _, mask_combined = cv2.threshold(mask_combined, 100, 255, cv2.THRESH_BINARY)
            kernel_erode = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 21))
            kernel_dilate = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (11, 21))
            mask_combined = cv2.erode(mask_combined, kernel_erode)
            mask_combined = cv2.dilate(mask_combined, kernel_dilate)
            mask_combined[mask_combined > 0] = int(142)

    # Perform a binary threshold the combined mask.
    _, mask_combined = cv2.threshold(mask_combined, threshold, 255, cv2.THRESH_BINARY)

    return mask_combined
\end{minted}
\caption[Perform Background Subtraction]{%
    The code used to perform background subtraction on an image.
}
\label{lst:perform_background_subtraction}
\end{listing}

\begin{listing}[H]
\begin{minted}[frame=lines, framerule=\heavyrulewidth, framesep=6pt, linenos, fontsize=\scriptsize]{python}
def find_contour(image: Image, min_size: int=600) -> Contour:
    """Find the largest silhouette contour within the image."""
    image = image.copy()

    # Aquire largest contour.
    _, contours, _ = cv2.findContours(image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    if len(contours) == 0:
        return None
    contour = max(contours, key=len)

    # Ensure contour meets minimum size.
    _, _, w, h = cv2.boundingRect(contour)
    if w * h < min_size:
        return None

    return contour
\end{minted}
\caption[Find a Contour Around a Silhouette]{%
    The method used to find a contour around a silhouette.
}
\label{lst:find_contour}
\end{listing}

\begin{listing}[H]
\begin{minted}[frame=lines, framerule=\heavyrulewidth, framesep=6pt, linenos, fontsize=\scriptsize]{python}
def locate_feet(bounding_box: Rectangle, contour: Contour, back_facing: bool) -> Feet:
    """Locate the left and right feet within an image."""
    if not bounding_box or contour is None:
        return None, None

    # Perform basic foot locating algorithm.
    x, y, w, h = bounding_box
    box_w, box_h = int(w * 0.35), int(h * 0.15)
    left_x, left_y = min(contour, key=lambda c: distance.euclidean(c, [x, y + h]))[0]
    left = (int(left_x - (box_w * 0.2)), int(left_y - (box_h * 0.8)), box_w, box_h)
    right_x, right_y = min(contour, key=lambda c: distance.euclidean(c, [x + w, y + h]))[0]
    right = (int(right_x - (box_w * 0.5)), int(right_y - (box_h * 0.8)), box_w, box_h)

    # Swap the foot labels if the person is foward facing.
    if not back_facing:
        left, right = right, left

    return left, right
\end{minted}
\caption[The Heuristic Aprroach Used to Locate Feet]{%
    The heuristic approach used to locate feet in an image.
}
\label{lst:locate_feet}
\end{listing}

\begin{listing}[H]
\begin{minted}[frame=lines, framerule=\heavyrulewidth, framesep=6pt, linenos, fontsize=\scriptsize]{python}
def are_inliers(left: Foot, right: Foot, history: List[Feet]) -> Tuple[bool, bool]:
    """Determine if both feet are inliers compared to their previous locations."""
    def is_inlier(foot: Foot, history: List[Foot], lookback: int=10, degree: int=1,
                  max_deviation: int=8) -> bool:
        """Determine if a foot is an inlier compared to its previous locations."""
        # Gather list of most recent foot locations.
        recent_history = list(reversed(history))[:lookback]
        recent_locations = [box[:2] for box in recent_history if box]

        # Seperate the x and y coordinates.
        if not recent_locations:
            return False
        xs, ys = map(list, zip(*recent_locations))
        if numpy.any(numpy.isnan(xs)) or numpy.any(numpy.isnan(ys)):
            return False

        # Create a polynomial model based of recent locations.
        coefficients = numpy.polyfit(xs, ys, degree)
        polynomial = numpy.poly1d(coefficients)

        # Determine if the foot's current position fits the model.
        if not foot:
            return False
        x, y = foot[:2]
        if abs(y - polynomial(x)) > max_deviation:
            return False

        return True

    # Perform inlier detection for both feet.
    left_result = is_inlier(left, [h[0] for h in history])
    right_result = is_inlier(right, [h[1] for h in history])

    return left_result, right_result
\end{minted}
\caption[The Method Used to Validate Detections]{%
    The method used to verify that detections fit historically.
}
\label{lst:are_inliers}
\end{listing}

