\chapter{Results}
\label{chap:results}
This chapter provides the results achieved from testing the system with the eight sequences.
Firstly, we outline the process of developing the ground truth labels for each video sequence.
With the ground truth labels we are then able to compare them with the detections made by the system.
Various scoring metrics are explored and explanations are provided to demonstrate the value of their comparison scores.



\section{The Ground Truth}
In any computer vision task, in order to measure the performance of a model the ground truth for each sample in the dataset must be developed.
The ground truth is a standard against which a model can evaluates its performance.
In this case the ground truth is labelled by hand through visual inspection of each image frame.
The human eye in combination with our brains is a fairly good standard to measure our model against as they are highly adapted to this task.
With that being said, we are by no means perfect and there are likely to be minor inaccuracies in the ground truth itself.

Frame by frame, the bounding regions for each foot in every sequence was labelled.
In each sequence, the feet were labelled from the first frame within which they appear to the last.
If a foot was occluded by the leg of another foot, its location is still labelled.
The feet were labelled manually every ten frames, and the intermediate values were interpolated.
All interpolated labels were inspected to ensure that they did not mislabel any frames, and were fixed if necessary.
Listing \autoref{lst:ground_truth_label_example} shows examples of how these labels were stored. 

\begin{listing}[H]
\begin{minted}[frame=lines, framerule=\heavyrulewidth, framesep=6pt, linenos]{text}
276,1,270.2,337.6,293.8,372.2
276,2,215.8,368.4,249.6,397
\end{minted}
\caption[Example of a Ground Truth Label]{%
    An example of the stored ground truth labels for each foot.
    The labels are stored in a \gls{csv} format, in the order of frame index, foot ID, the top left x and y coordinates, and the bottom right x and y coordinates.
    The foot IDs are \mintinline{text}{1} for the left, and \mintinline{text}{2} for the right.
}
\label{lst:ground_truth_label_example}
\end{listing}

Once both the model's prediction and the ground truth label are available they both can be compared with one another.
As we can see from \autoref{fig:ground_truth_comparison}, what previously looked like a good detection is a little off from the labelled ground truth for that image frame.
This is not necessarily purely the fault of the model, but it is an example of how our definition of a good detection differs from what denotes a `true' foot.

When examining \autoref{fig:example_foot_detections}, we can see that the right detection, for example, fully encompasses the foot, however its overlap with the ground truth is significantly smaller proportionally.
This is due to the ground truth being a tighter bounding box than the prediction.
Should we penalise the model for this, even though it seems to successfully cover the foot?
This is the problem that we need to be aware of when we attempt to score our performance. 

\begin{figure}[H]
\centering
\includegraphics[width=0.8\textwidth]{ground_truth_comparison}
\caption[Ground Truth Comparison]{
    A visual comparison between the predicted bounding region and the ground truth label.
    For this image, the white boxes are the ground truth region, red is for the left intersection, and blue is for the right intersection.
}
\label{fig:ground_truth_comparison}
\end{figure}



\section{Scoring Metrics}
There are a few features you can elicit from a ground truth comparison such as that of the right foot in \autoref{fig:ground_truth_comparison}.
For example, there is the area of either bounding regions, the area of the union of the two, and the area of the intersection.
We need to determine how we can use these features to create an acceptable scoring metric.

If we want to avoid the previously mentioned issue entirely, we can use a simple metric which tells us whether or not the two bounding regions overlap, giving a high score if they do, and a low score if they do not.
The formula for this is as follows:

\begin{equation}
H=
\begin{cases}
1, & \text{if}\ A\cap B \neq \emptyset \\
0, & \text{otherwise}
\end{cases}
\end{equation}
 
Where $H$ is the hit rate, $A$ is the first region, and $B$ is the second.
This would give us a $1$ for the right foot in \autoref{fig:ground_truth_comparison}, which is what we want, and a $0$ if we move the prediction such that it no longer overlaps with the ground truth, which is also what we want.
The problem that arises is that we can always score a $1$ on this metric by simply creating a large bounding region over the entire image.

This does not mean that hit rate is not a useful metric, however, it means that we need an additional metric to show how well a detection overlaps with the ground truth, when it actually overlaps.
Thada and Jaglan provide a comparison of three different scoring metrics wherein two are applicable for our purposes, the Jaccard and Dice coefficients~\cite{thada2013comparison}.
The first of these two additional metrics, the Jaccard coefficient, goes as follows:

\begin{equation}
J=\frac{|A\cap B|}{|A\cup B|}
\end{equation}

Where $J$ is the Jaccard coefficient, $A$ is the first region, and $B$ is the second region.
The second of the additional metrics is the Dice coefficient.
This metric is defined in the following way:

\begin{equation}
D=2\frac{|A\cap B|}{|A|+|B|}
\end{equation}

Where $D$ is the Dice coefficient, $A$ is the first region, and $B$ is the second region.
Both the Jaccard and Dice coefficients are sufficient for evaluating the quality of overlap, however both will be used to aid in reducing any bias introduced by using either of them separately.

The scoring metrics we have defined thus far are the hit rate, Jaccard coefficient, and the Dice coefficient.
The hit rate determines the accuracy of the detection, and the Jaccard and Dice coefficients determine the precision and quality of the predicted bounding box.
These equations are for the general application of these metrics, \autoref{tab:scoring_metrics_for_image_frame} contains the version of these metrics tailored for our use case, as these metrics need to be calculated for each foot within every labelled image frame.

\begin{table}[H]
\centering
\begin{tabular}{@{}lll@{}}
	\toprule
    Metric              & Symbol & Equation \\
	\midrule
    Hit Rate            & $H_i$     & \Scale[0.65]{\begin{cases}1, &\text{if}\ P_i\cap{} G_i\neq{} \emptyset{} \\0, & \text{otherwise}\end{cases}} \\[3mm]
    Jaccard Coefficient & $J_i$     & $\frac{|P_i\cap G_i|}{|P_i\cup G_i|}$ \\[2mm]
    Dice Coefficient    & $D_i$     & $2\frac{|P_i\cap G_i|}{|P_i|+|G_i|}$ \\[1mm]
	\bottomrule
\end{tabular}
\caption[Scoring Metrics for Image Frames]{
    The different metrics used to score each image frame in a sequence, where $i$ is the frame index, $P$ is the predicted bounding region, and $G$ is for the labelled ground truth region.
}
\label{tab:scoring_metrics_for_image_frame}
\end{table}

In order to evaluate the video sequence as a whole we need to do more than just average the scores achieved over the entire sequence, as for both of the coefficients, a missed detection is always a zero.
If we want to use the coefficients to measure the quality of detections, we must only average over those frames in which an accurate detection was made, or rather, frames wherein there exists an intersection.
\autoref{tab:scoring_metrics_for_video_sequence} shows the formulas used to calculate the scores for each of the available metrics.

It is also important to calculate these scores separately for each foot as they both tend to enter and exit the video sequence in different frames, and thus, have different counts.
After each score metric is calculated, for each foot, they can then be averaged together in order to provide a single score for each metric for the entire video sequence.

\begin{table}[H]
\centering
\begin{tabular}{@{}lll@{}}
	\toprule
    Metric              & Symbol & Equation \\
	\midrule
    Hit Rate            & $H_s$      & $\frac{\sum_{i=0}^n H_i}{n}$ \\[2mm]
    Jaccard Coefficient & $J_s$      & $\frac{\sum_{i=0}^n H_i J_i}{\sum_{i=0}^n H_i}$ \\[3mm]
    Dice Coefficient    & $D_s$      & $\frac{\sum_{i=0}^n H_i D_i}{\sum_{i=0}^n H_i}$ \\[1mm]
	\bottomrule
\end{tabular}
\caption[Scoring Metrics for Video Sequences]{
    Scoring metrics used for each video sequence, where $s$ is the sequence ID, $i$ is the index of each frame, and $n$ is the total number of labelled frames in the video sequence.

}
\label{tab:scoring_metrics_for_video_sequence}
\end{table}



\section{Assessing Performance}
Each of the eight video sequences were scored using the above criteria.
For each sequence, an average score on each of the metrics was computed for each foot, and then the average scores for each foot were averaged into a single score for the entire sequence.
The scores achieved on each metric by each video sequence is listed in \autoref{tab:image_sequence_details}.
On average, over all eight video sequences, the hit rate score is $0.61$, the Jaccard coefficient is $0.33$, and the Dice coefficient is $0.46$.
The meaning of this shall be discussed later on, but first we should look more into the individual sequences so that we can understand what is going on.

\begin{table}[H]
\centering
\begin{tabular}{@{}llll@{}}
	\toprule
	Sequence ID & Hit Rate & Jaccard Coefficient & Dice Coefficient \\
	\midrule                                         
	1           &     0.83 &                0.34 &             0.48 \\
	2           &     0.60 &                0.30 &             0.44 \\
	3           &     0.54 &                0.30 &             0.42 \\
	4           &     0.74 &                0.29 &             0.42 \\
	5           &     0.62 &                0.29 &             0.42 \\
	6           &     0.78 &                0.28 &             0.41 \\
	7           &     0.28 &                0.38 &             0.52 \\
	8           &     0.47 &                0.42 &             0.56 \\
	\bottomrule
\end{tabular}
\caption[Image Sequence Metric Scores]{
    The final scores achieved by the model on each of the sequences in the dataset.
    This includes the hit rate, the Jaccard and Dice coefficient.
}
\label{tab:image_sequence_details}
\end{table}

In order to aid in the interpretation of these results, the scores for each metric within each sequence has been plotted in \autoref{fig:overall_metric_scores}.
From this graph we can see that the model achieves a good hit rate on a few sequences (1, 4, 6), however does quite badly on others (3, 7, 8).
As mentioned previously, this means that the accuracy across frames seems to be suffering in some areas.
What is actually happening is that the background subtraction methods tend to lose the bottom half of the silhouette in some cases where the pedestrian strays too far away from the camera's position.

This issue is more prominent in some sequences than in others, most likely do to the speed at which the individuals walk and the color of their clothing.
For example in sequence $7$ the individual has gray jeans which are similarly colored to the floor, making it hard for the background subtraction algorithms to create a good and consistent silhouette.

The other thing to note is that both the Jaccard and Dice coefficients are about the same for each dataset, except for $7$ and $8$.
This means that when we are accurate, we are getting fairly good quality scores, which means we are not cheating on the hit rate by creating larger boxes.
Additionally, $7$ and $8$ have slightly higher quality measures, because the bottom portion of the silhouette was missing for a significant amount of time during the distant frames, but this also suggests that when the pedestrians are closer our algorithms perform better, as the majority of the valid detections where in the frames with the silhouette nearest the camera.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{metric_scores}
\caption[Graph of the Metric Scores]{
    A graph of the metric scores achieved by the system on each image sequence.
    Each bar corresponds to the values listed within \autoref{tab:image_sequence_details}.
}
\label{fig:overall_metric_scores}
\end{figure}

