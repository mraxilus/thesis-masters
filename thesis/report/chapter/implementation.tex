% chktex-file 24
\chapter{Implementation}
\label{chap:implementation}
This chapter covers the various aspects of the implementation of the system.
We first provide an overview and description of the dataset used by this project, and move on to outline the various pre-processing steps performed on each image frame.
Afterwards, this chapter focuses on the techniques used to combine various background subtraction methods, in an effort to alleviate reflection issues.
Finally, we cover the heuristic approach used to track the feet positions of silhouettes over time.



\section{Dataset Overview}
As previously mentioned, this dataset was acquired from the recordings previously made by Doughty et al.
More specifically, our footage is a subset of their entire dataset, in so far as it only includes one of the various different floor patterns.
The footage consists of university personnel walking both up and down the corridor as they normally would.
The original video was approximately half an hour of recording, occupying about four gigabytes of space, as can be seen in \autoref{tab:original_video_details}.

\begin{table}[H]
\centering
\begin{tabular}{@{}llrr@{}}
	\toprule
    Name          & Codec                & Duration (mm:ss) & Size (MB) \\
	\midrule
    recording.mp4 & H.264 (High Profile) &            26:30 &   4,001.7 \\
	\bottomrule
\end{tabular}
\caption[Original Video Details]{
	The details of the video footage originally recorded by Doughty et al.~\cite{doughty2015automatic}.
	This footage was captured using a GoPro camera at 30 frames per second, with a resolution of 1280$\times$720 pixels.
}
\label{tab:original_video_details}
\end{table}

A high-camera angle was needed so that both the floor and the individuals could be seen as much as possible.
Much personnel appear within the footage, however some of them are unsuitable for our use case, as it was not a controlled environment.
This project focuses on tracking a single individual at a time, without visual obstructions such as large bags and carts.
\autoref{fig:bad_examples_from_dataset} demonstrates some potential issues that invalidates a particular example from being considered.
Additionally, at times some examples are too temporally close to other examples.
This becomes an issue because some background subtraction methods need a few frames to develop their initial background model, and because of this, each example needs a small period of quiet time before they enter the corridor.

\begin{figure}[H]
\centering
\begin{subfigure}[b]{0.49\textwidth}
	\centering
	\includegraphics[width=\textwidth]{bad_examples_from_dataset/large_object}
    \caption{Large object.}
\end{subfigure}
\hfill
\begin{subfigure}[b]{0.49\textwidth}
	\centering
	\includegraphics[width=\textwidth]{bad_examples_from_dataset/multiple_people}
    \caption{Multiple people.}
\end{subfigure}
\caption[Bad Examples From Dataset]{
    Examples of sequences which were rejected due to being outside the scope of this project.
    Possible reasons for rejection include there being a large object in the sequence, and there being multiple pedestrians present.
}
\label{fig:bad_examples_from_dataset}
\end{figure}

When we are looking for video sequences there are a few requirements.
The sequence must contain a single individual walking through the corridor without changing direction.
It must also have a small period of quiet time before the individual enters, where quiet time consists of sequences where only the background is in the scene.
Finally, the individual must not be carrying any object which significantly obscures the shape of their silhouette.
Some examples of sequences which meet these requirements can be seen in \autoref{fig:good_examples_from_dataset}.
Additionally, the individual's direction of travel should not matter for the purposes of our system.

\begin{figure}[H]
\centering
\begin{subfigure}[b]{0.49\textwidth}
	\centering
	\includegraphics[width=\textwidth]{good_examples_from_dataset/toward}
    \caption{Facing towards.}
\end{subfigure}
\hfill
\begin{subfigure}[b]{0.49\textwidth}
	\centering
	\includegraphics[width=\textwidth]{good_examples_from_dataset/away}
    \caption{Facing away.}
\end{subfigure}
\caption[Good Examples From Dataset]{
    Examples of sequences which embody the focus of this project.
    This project deals with tracking individual pedestrians, whilst also being able to cope with differences in their direction of travel.
}
\label{fig:good_examples_from_dataset}
\end{figure}

After the criteria was identified, the footage needed to be split into smaller segments which consist of one event each.
To accomplish this, the entire video was watched from start to finish noting down both start and end times for a time segment within which our requirements were met.
Extracting these video segments required the use of a terminal based multimedia data manipulation software such as FFmpeg~\cite{ffmpeg}.

FFmpeg allows us to split the original footage into smaller videos which run between the timestamps elicited previously.
After generating the video, FFmpeg also allows us to convert every frame in each sequence into an image.
This is important for a few reasons.
The first reason is that it ensures that each frame grabbed from the sequence will always be the same for a given index, and because of this after labelling each image with our ground truth, both the image index and the ground truth index is guaranteed to refer to the same video frame. 
The commands used for this are shown in Listing~\autoref{lst:convert_video_to_image_sequence}.

\begin{listing}[H]
\begin{minted}[frame=lines, framerule=\heavyrulewidth, framesep=6pt, linenos]{bash}
# Segment video into shorter sequences. 
ffmpeg -ss START -i recording.mp4 -t DURATION SEQUENCE_ID.mp4

# Convert video to image sequence.
mkdir SEQUENCE_ID
ffmpeg -i SEQUENCE_ID.mp4 -qscale:v 2 SEQUENCE_ID/%03d.jpg
\end{minted}
\caption[Convert Video to Image Sequence]{%
    The Bash commands used to produce the image sequences for each video example.
    Where \mintinline{bash}{START} is the start time, \mintinline{bash}{DURATION} is the length to include from the start point, and \mintinline{bash}{SEQUENCE_ID} is the ID number of the video sequence.
    Both \mintinline{bash}{START} and \mintinline{bash}{DURATION} are of the format HH:mm:ss.
}
\label{lst:convert_video_to_image_sequence}
\end{listing}

After combing through the original recording for valid examples and using the commands in Listing~\autoref{lst:convert_video_to_image_sequence}, multiple image sequences where obtained. 
Unfortunately, the distribution between videos in which pedestrians were walking away from or towards the camera was heavily skewed on the side of the towards direction.
Due to this, some `towards' sequences where dropped to maintain an even distribution between the two.

Even after dropping some sequences, eight remained and more information about them is laid out in~\autoref{tab:image_sequence_details}.
Each of these sequences where given an ID from $1$ to $8$ and their total number of frames ranges from approximately two hundred and fifty to four hundred.
In total there are four sequences where the pedestrian's direction of travel is towards the camera, and four sequences where it is away from the camera.

\begin{table}[H]
\centering
\begin{tabular}{@{}lll@{}}
	\toprule
	Sequence ID & Frame Count & Walking Direction \\
	\midrule                                         
	1          &         321 &            Toward  \\
	2          &         336 &              Away  \\
	3          &         290 &              Away  \\
	4          &         274 &              Away  \\
	5          &         359 &            Toward  \\
	6          &         406 &              Away  \\
	7          &         274 &            Toward  \\
	8          &         299 &            Toward  \\
	\bottomrule
\end{tabular}
\caption[Image Sequence Details]{
	The details of each image sequence in the dataset.
	Each sequence has an ID, a total frame count, and a direction.
	The direction specifies whether the pedestrian is walking either towards or away from the camera.
}
\label{tab:image_sequence_details}
\end{table}



\section{Pre-Processing}
For the remainder of this report, a single frame taken from Sequence $1$  will be used to demonstrate each technique.
This should help with seeing the relationship between each part of the system, as it shows how a singular frame passes through the entire system.
Other sequences could have been used to show how the system performs in different contexts, however this was a deliberate choice in an effort to improve understandability.
\autoref{fig:original_example_image} presents the original image frame that will be used for this purpose.

\begin{figure}[H]
\centering
\includegraphics[width=0.8\textwidth]{original_example_image}
\caption[Original Example Image]{
    An example image frame from sequence $1$.
    This is near the end of the sequence, wherein the pedestrian is walking towards the camera.
}
\label{fig:original_example_image}
\end{figure}

As with most recording devices, the GoPro camera picks up noise which is present among all the image frames which can be seen in~\autoref{fig:denoising_example_a}.
Because the background subtraction methods develop a model of the background, we want to make the background as consistent as possible.
To achieve this, each image frame is first passed through a denoising algorithm in order to acquire better results from later stages, such as background subtraction.

The method we use here is the non-local means denoising algorithm provided by OpenCV.\@
It works by using a sliding widow approach to locate similar regions within an image.
For each region it averages the pixel values with those of similar regions so that, on average, the noise will be reduced.
The algorithm can not only work for regions in the same image, but across images in a sequence as well~\cite{bradski2000opencv}.
This is the main reason for choosing this method, as we want to remove the frame-to-frame noise on the background scene.

The downside to non-local means denoising is that it is quite computationally expensive compared to other noise removal techniques such as blurring.
This is especially true if we decided to perform the process over a window of images as opposed on a single frame.
Fortunately, real-time processing is not a requirement of this system, thus non-local means denoising was used as is observed in~\autoref{fig:denoising_example}.
Ultimately, the search window was restricted to the single frame in question, as using a window across frames only provided a negligible improvement, whilst severely increasing time complexity.

\begin{figure}[H]
\centering
\begin{subfigure}[b]{0.49\textwidth}
	\centering
	\includegraphics[width=\textwidth]{denoising/before}
    \caption{Before denoising.}
    \label{fig:denoising_example_a}
\end{subfigure}
\hfill
\begin{subfigure}[b]{0.49\textwidth}
	\centering
	\includegraphics[width=\textwidth]{denoising/after}
    \caption{After denoising.}
\end{subfigure}
\caption[Denoising Example]{
    A sub-region within the original image seen in \autoref{fig:original_example_image}, both before and after denoising.
    The resulting image was produced by the colored non-local means denoising algorithm implemented by the OpenCV library~\cite{bradski2000opencv}.
}
\label{fig:denoising_example}
\end{figure}

When performing the pre-processing steps on a sequence, a single image is loaded at a time.
To save computational resources the denoised image is saved for future use, so that subsequent runs of the same dataset runs significantly faster.
If the system has not encountered the image before, it is denoised and stored in a cache for future use.
The system first checks to see if the image is stored already, and if so, it loads the denoised image from the cache. This process is presented in~\autoref{fig:preprocessing_overview}.
Bear in mind that this will not save computational resources on novel instances.

\begin{figure}[H]
\centering
\begin{tikzpicture}[node distance=2cm]
    \node (input) [inputOutput] {Input Image};
    \node (cached) [decision, below of=input] {Cached?};
    \node (denoised) [inputOutput, below of=cached] {Denoised Image};
    \node (store) [box, left of=denoised, xshift=-1.5cm] {Denoise and Store};
    \node (load) [box, right of=denoised, xshift=1.5cm] {Load from Cache};
    \node (output) [inputOutput, below of=denoised] {Output Image};

	\draw [arrow] (input) -- (cached);
    \draw [arrow] (cached) -| node[above, xshift=0.5cm] {No} (store);
    \draw [arrow] (cached) -| node[above, xshift=-0.5cm] {Yes} (load);
    \draw [arrow] (store) -- (denoised);
    \draw [arrow] (denoised) -- (load);
    \draw [arrow] (store) |- (output);
    \draw [arrow] (load) |- (output);
\end{tikzpicture}
\caption[Overview of the Pre-Processing Procedure]{%
    A graphical overview of the pre-processing procedure used by the system, where each trapezium represents a datum, diamonds represent control logic, and rectangles represent function operations.
}
\label{fig:preprocessing_overview}
\end{figure}



\section{Background Subtraction}
The most integral component of this system is background subtraction.
Background subtraction separates the foreground, or the human silhouette, from the background scene.
In an ideal scenario, a background subtraction method should show the clean and crisp outline of the silhouette of each human walking through the corridor.
In reality, this is far from the case.

Most methods are unable to distinguish the movement of an object, from the movement of the shadow cast by that same object.
Some techniques are able to provide shadow detection, but even with that, this does not alleviate the issue caused by reflections on the walls and floor.
Unfortunately for us, this dataset is plagued by bright reflections on both the floor, and the glass panels on the left wall.
The wall, for the most part, does not interfere with the silhouette, however the floor causes significant shadowing in a large majority of background subtraction methods, as can be seen in~\autoref{fig:bad_examples_of_background_subtraction_algorithms_b}.

The parameters for a total of thirty-four different background subtraction methods, provided by the BGSLibrary, were tuned on the sample video sequences.
Not a single one was able to provide a consistent silhouette throughout the corridor, without encountering an issue.
\autoref{fig:bad_examples_of_background_subtraction_algorithms} shows the three main issues that were consistently encountered.

More simplistic models tended to have incomplete silhouettes; complex models tended to fail on the floor reflections and stripes; however some methods fared well, but represented the silhouette as a morphing blob, an example of which is~\autoref{fig:bad_examples_of_background_subtraction_algorithms_c}.
All of these issues usually did not tend occur together in the same algorithm, but there was no one algorithm which handled all of these issues to a satisfactory standard.

\begin{figure}[H]
\centering
\begin{subfigure}[b]{0.32\textwidth}
	\centering
	\includegraphics[width=\textwidth]{bad_background_subtraction/frame_difference}
    \caption{Incomplete silhouette.}
\end{subfigure}
\hfill
\begin{subfigure}[b]{0.32\textwidth}
	\centering
	\includegraphics[width=\textwidth]{bad_background_subtraction/gmm}
    \caption{Reflection issues.}
    \label{fig:bad_examples_of_background_subtraction_algorithms_b}
\end{subfigure}
\hfill
\begin{subfigure}[b]{0.32\textwidth}
	\centering
	\includegraphics[width=\textwidth]{bad_background_subtraction/subsense}
    \caption{Lack of detail.}
    \label{fig:bad_examples_of_background_subtraction_algorithms_c}
\end{subfigure}
\caption[Bad Examples of Background Subtraction Algorithms]{
    Examples of foreground masks from background subtraction algorithms that were rejected.
    Respectively, these are frame difference, \gls{gmm}, and SubSENSE.\@
    These masks demonstrate common issues produced by many of the available techniques, even after excessive parameter tuning.
}
\label{fig:bad_examples_of_background_subtraction_algorithms}
\end{figure}

Because each algorithm failed in a different area, it was likely that an ensemble of multiple methods would provide a better results.
By combining multiple methods which encountered different issues, the hope was that on average they would cancel each other's issues out.
After experimenting with many different combinations, \autoref{fig:selected_background_subtraction_algorithms} shows foreground masks for the three methods which ultimately ended up being used.

\begin{figure}[H]
\centering
\begin{subfigure}[b]{0.32\textwidth}
	\centering
	\includegraphics[width=\textwidth]{background_subtraction/kde}
    \caption{\gls{kde}.}
\end{subfigure}
\hfill
\begin{subfigure}[b]{0.32\textwidth}
	\centering
	\includegraphics[width=\textwidth]{background_subtraction/mog}
    \caption{\gls{mog}.}
\end{subfigure}
\hfill
\begin{subfigure}[b]{0.32\textwidth}
	\centering
	\includegraphics[width=\textwidth]{background_subtraction/fuzzy_gaussian}
    \caption{Fuzzy Gaussian.}
\end{subfigure}
\caption[Selected Background Subtraction Algorithms]{
    The foreground masks produced by the three different background subtraction techniques used from the BGSLibrary~\cite{sobral2013bgslibrary}.
}
\label{fig:selected_background_subtraction_algorithms}
\end{figure}

To combine the foreground masks produced by each algorithm, the masks were added together in a weighted fashion.
Basically, each of the masks votes for the pixels that it believes is the true mask, and the values of the final mask, rather than being binary, ranges from zero to three. Zero being no masks voted for a pixel, and three being all masks voted for a pixel.
Mathematically, this operation is described as follows:

\begin{equation}
\sum_{i=0}^{n}\frac{1}{n}* I_i
\end{equation}

Where $i$ is the index of the mask, $n$ is the total number of masks, and $I$ is the binary image representing the mask.
In a lot of cases, applying this operation is still not enough to remove the issues caused by floor reflections.
Alleviating this required the use of morphological transforms, namely erosion followed by dilation, which is more commonly called opening.
The kernels for these operations where tuned specifically to remove the encountered reflections.
This is followed by a second pass of the weighted addition formula shown above.
After combining the masks the image is thresholded in order to fully remove artifacts such as noise and reflections.
The end result of all of these operations, both before and after thresholding, is shown in \autoref{fig:combined_and_thresholded_foreground_masks}.

\begin{figure}[H]
\centering
\begin{subfigure}[b]{0.49\textwidth}
	\centering
	\includegraphics[width=\textwidth]{background_subtraction/combined}
    \caption{Combined.}
\end{subfigure}
\hfill
\begin{subfigure}[b]{0.49\textwidth}
	\centering
	\includegraphics[width=\textwidth]{background_subtraction/thresholded}
    \caption{Thresholded.}
    \label{fig:combined_and_thresholded_foreground_masks_b}
\end{subfigure}
\caption[Combined and Thresholded Foreground Masks]{
    The image generated from combining the foreground masks produced by the three methods in \autoref{fig:selected_background_subtraction_algorithms}, both before and after thresholding.
}
\label{fig:combined_and_thresholded_foreground_masks}
\end{figure}

The background subtraction portion of this system consists of a two-pass process.
First, the input image is received after pre-processing and is passed to each of the three background subtraction algorithms, namely \gls{kde}, \gls{mog}, and Fuzzy Gaussian.
Each of these algorithms are then combined into one mask by weighted addition, producing a combined mask. This concludes the first pass through this part of the system.

The combined image is then pass through a morphological opening filter to produce a mask that represents the parts of the image where the human silhouette, and not the reflection, is likely to occur.
The three masks are then recombined with this newly developed opened mask to produce the final output image, which is then thresholded to produce a binary mask.
This concludes the second and final pass through this portion of the system.
A graphical representation of these processes is provided in \autoref{fig:background_subtraction_procedure}.

\begin{figure}[H]
\centering
\begin{tikzpicture}[node distance=2cm]
    \node (input) [inputOutput] {Input Image};
    \node (generate) [boxMedium, below of=input] {Generate Masks};
    \node (mog) [inputOutput, below of=generate] {\gls{mog} Mask};
    \node (kde) [inputOutput, left of=mog, xshift=-1cm] {\gls{kde} Mask};
    \node (fuzzy) [inputOutputMedium, right of=mog, xshift=1.5cm] {Fuzzy Gaussian Mask};
    \node (sum) [boxMedium, below of=mog] {$\sum_{i=0}^{n}\frac{1}{n}* I_i$};
    \node (pass) [decision, below of=sum, yshift=-1.5cm, xshift=3.5cm] {First~Pass?};
    \node (output) [inputOutput, below of=pass, yshift=-0.5cm] {Output Mask};
    \node (opening) [box, left of=pass, xshift=-4.5cm] {Opening};
    \node (opened) [inputOutput, below of=sum] {Opened Mask};

    \draw [arrow] (input) -- (generate);
    \draw [arrow] (generate) -- (mog);
    \draw [arrow] (generate) -| (kde);
    \draw [arrow] (generate) -| (fuzzy);
    \draw [arrow] (mog) -- (sum);
    \draw [arrow] (kde) |- (sum);
    \draw [arrow] (fuzzy) |- ([yshift=0.3cm]sum);
    \draw [arrow] ([yshift=-0.3cm]sum) -| (pass);
    \draw [arrow] (pass) -- node[right] {No} (output);
    \draw [arrow] (pass) -- node[above] {Yes} (opening);
    \draw [arrow] (opening) |- (opened);
    \draw [arrow] (opened) -- (sum);
\end{tikzpicture}
\caption[Overview of the Background Subtraction Procedure]{%
    A diagram showing the method used for performing background subtraction.
    One thing to note is that the Opened Mask node is not included in the first pass of the weighted addition, as it is generated after the fact.
}
\label{fig:background_subtraction_procedure}
\end{figure}



\section{Feet Tracking}
After the foreground mask is produced we can then utilise techniques to locate and track the pedestrian's feet over time.
The first step in this process is to find the contour of the silhouette, which allows us to then heuristically locate the position of both feet.
The contour consists of a subset of the silhouette's pixels that fully encompass the inner pixels.
Put simply, the contour is an outline of the silhouette found within the thresholded masks produced by our background subtraction procedure.
\autoref{fig:contour_generation} shows the contour produced from the thresholded mask in \autoref{fig:combined_and_thresholded_foreground_masks_b}, overlaid on the original image frame from sequence $1$.

\begin{figure}[H]
\centering
\includegraphics[width=0.8\textwidth]{contour_generation}
\caption[Contour Generation]{
    The contour generated from the silhouette produced by the background subtraction methods.
    Each pixel along the contour is displayed in blue with a thickness of three pixels, in order to make them more visible.
}
\label{fig:contour_generation}
\end{figure}

From the contour the locations of the feet are assumed to be the pixels of the contour closest to the points assumed to be the most likely, based on the dimensions of the contour.
These most likely points are approximately the lower left and right-hand corners.
The size of the bounding regions for the foot detections are based on the height and width proportions of the contour.

At this point in the process, it is ambiguous as to which of the foot detections are either the left or right foot.
To disambiguate this, a history of each and every detection is kept and consulted whenever new detections are made.
Originally, the foot on the left side of the image is assumed to be the left foot, and the foot on the right side of the image is assumed to be the right, that is, it assumes the pedestrian is walking away from the camera.
Once a few detections have been made the system can create a motion vector and then correct the orientation accordingly, \autoref{fig:example_foot_detections} is an example of this.

From these operations alone the system is simply detecting the feet in every frame independent of each other, and is not utilising previous information for anything other that disambiguating each foot.
What ends up occurring is that the detections are jittery and jump around wildly do to silhouette noise.
To deal with this issue, each detection is validated against previous detections using a polynomial model with a fixed window size.
The model looks at the most recent detections and from those attempts to predict where the current position of each foot should be.
If the detection deviates to far from the prediction, then the detection is marked as invalid.

As the system progresses through the frames in the sequence, it cleans up the history by interpolating between the validated foot detections on both the location and size parameters.
This, in effect, smooths out the trajectories and sizes of detections, and gets rid of sudden and large jitters caused by noise in the background subtraction process.

\begin{figure}[H]
\centering
\includegraphics[width=0.8\textwidth]{example_detections}
\caption[Example Foot Detections]{
    The foot detections made by the system.
    Each foot's location and dimensions are estimated then labelled separately, where the yellow bounded region is for the left foot's detection, and the green is for the right.
}
\label{fig:example_foot_detections}
\end{figure}

The feet tracking component of this system requires as inputs, the foreground mask of the current image frame, and a history of past foot locations.
This component then locates the outline of the pedestrian's silhouette, producing a set of points which defines the contour. 
The contour is then used to detect both feet within the original image frame, and these detections are then validated against a window of historical foot locations.
These detections are then marked as being valid or invalided and are added to the detection history for use by the next frame in the video sequence.
This entire process is graphically summarised in \autoref{fig:feet_tracking_procedure}.


\begin{figure}[H]
\centering
\begin{tikzpicture}[node distance=2cm]
    \node (input) [inputOutput] {Input Mask};
    \node (locate) [box, below of=input] {Locate Contour};
    \node (contour) [inputOutput, right of=locate, xshift=2cm] {Contour Points};
    \begin{pgfonlayer}{second}
        \node (contour1) [inputOutput, right of=locate, xshift=1.9cm, yshift=1mm] {\color{white}{Contour Points}};
    \end{pgfonlayer}
    \begin{pgfonlayer}{third}
        \node (contour2) [inputOutput, right of=locate, xshift=1.8cm, yshift=2mm] {\color{white}{Contour Points}};
    \end{pgfonlayer}
    \node (detect) [box, below of=contour] {Detect Feet};
    \node (detections) [inputOutput, below of=locate] {Foot Detections};
    \begin{pgfonlayer}{second}
        \node (detections1) [inputOutput, below of=locate, xshift=-1mm, yshift=1mm] {Foot Detections};
    \end{pgfonlayer}
    \node (history) [inputOutput, left of=input, xshift=-2cm] {Input History};
    \begin{pgfonlayer}{second}
        \node (history1) [inputOutput, left of=input, xshift=-2.1cm, yshift=1mm] {\color{white}{Input History}};
    \end{pgfonlayer}
    \begin{pgfonlayer}{third}
        \node (history2) [inputOutput, left of=input, xshift=-2.2cm, yshift=2mm] {\color{white}{Input History}};
    \end{pgfonlayer}
    \node (validate) [box, below of=detections] {Validate Detections};
    \node (output) [inputOutput, below of=validate] {Output History};
    \begin{pgfonlayer}{second}
        \node (output1) [inputOutput, below of=validate, xshift=-1mm, yshift=1mm] {Output History};
    \end{pgfonlayer}
    \begin{pgfonlayer}{third}
        \node (output2) [inputOutput, below of=validate, xshift=-2mm, yshift=2mm] {Output History};
    \end{pgfonlayer}

    \draw [arrow] (input) -- (locate);
    \draw [arrow] (locate) -- (contour-|contour2.west);
    \draw [arrow] (contour) -- (detect);
    \draw [arrow] (detect) -- (detections);
    \draw [arrow] (detections) -- (validate);
    \draw [arrow] (validate) -- (output2.north-|output);
    \draw [arrow] (history) |- (validate);
\end{tikzpicture}
\caption[Overview of the Feet Tracking Procedure]{%
    An overview of feet tracking strategy employed by the system.
    Two overlapping trapeziums represent exactly two data entities, whereas three overlapping trapeziums represent multiple objects.
    Note that the Input History does not exist for the first image processed by the system.
}
\label{fig:feet_tracking_procedure}
\end{figure}



\section{High-Level Overview}
Now that we have run through the implementation of each component within the system, it would be useful to step back to describe the system as a whole.
Each video sequence is present to the system as a series of numbered image frames.
Every frame passes through the system one at a time in numerical order.
First, the images are pre-processed by going through a colored non-local means denoising filter, and a cached for subsequent usage.

Background subtraction is then performed on these denoised images.
The image is run through three different algorithms and then combined in a two-pass process than involves morphological transformations, which help to remove reflection issues.
The combined image is then thresholded to produce the final foreground mask which is used to track the feet of the pedestrian.

The foreground masks are then used to locate the possible foot locations within each image.
First, a series of points representing the contour of the silhouette are generated.
The contour is then used to determine both the location and size of both feet, which forms the detected bounding region.
To smooth and remove erroneous detections, detections in each frame are compared to the historical foot locations.

Invalid detections are later replaced with interpolates between the latest and last valid detections, up until the end of the sequence.
The resulting output of this system is a series of bounding boxes for each foot within each image, which attempt to identify the location of the left and right feet.
\autoref{fig:high_level_system_overview} provides an overview of the entire system.


\begin{figure}[H]
\centering
\begin{tikzpicture}[node distance=2cm]
    \node (input) [inputOutput] {Input Images};
    \begin{pgfonlayer}{second}
        \node (input1) [inputOutput, xshift=-1mm, yshift=1mm] {\color{white}{Input Images}};
    \end{pgfonlayer}
    \begin{pgfonlayer}{third}
        \node (input2) [inputOutput, xshift=-2mm, yshift=2mm] {\color{white}{Input Images}};
    \end{pgfonlayer}
    \node (processing) [boxMedium, below of=input] {Pre-Processing};
    \node (denoised) [inputOutput, right of=processing, xshift=2.5cm] {Denoised Images};
    \begin{pgfonlayer}{second}
        \node (denoised1) [inputOutput, right of=processing, xshift=2.4cm, yshift=1mm] {Denoised Images};
    \end{pgfonlayer}
    \begin{pgfonlayer}{third}
        \node (denoised2) [inputOutput, right of=processing, xshift=2.3cm, yshift=2mm] {Denoised Images};
    \end{pgfonlayer}
    \node (background) [boxMedium, below of=denoised] {Background Subtraction};
    \node (masks) [inputOutput, below of=processing] {Foreground Masks};
    \begin{pgfonlayer}{second}
        \node (masks1) [inputOutput, below of=processing, xshift=-1mm, yshift=1mm] {Foreground Masks};
    \end{pgfonlayer}
    \begin{pgfonlayer}{third}
        \node (masks2) [inputOutput, below of=processing, xshift=-2mm, yshift=2mm] {Foreground Masks};
    \end{pgfonlayer}
    \node (tracking) [box, below of=masks] {Feet Tracking};
    \node (detections) [inputOutput, below of=tracking] {Foot Detections};
    \begin{pgfonlayer}{second}
        \node (detections1) [inputOutput, below of=tracking, xshift=-1mm, yshift=1mm] {Foot Detections};
    \end{pgfonlayer}
    \begin{pgfonlayer}{third}
        \node (detections2) [inputOutput, below of=tracking, xshift=-2mm, yshift=2mm] {Foot Detections};
    \end{pgfonlayer}

    \draw [arrow] (input) -- (processing);
    \draw [arrow] (processing) -- (denoised-|denoised2.west);
    \draw [arrow] (denoised) -- (background);
    \draw [arrow] (background) -- (masks);
    \draw [arrow] (masks) -- (tracking);
    \draw [arrow] (tracking) -- (detections2.north-|detections);
    \draw [arrow] (detections.east) -| ++(6mm,0) |- (tracking.east);
\end{tikzpicture}
\caption[A High-Level Overview of the System]{%
    A high-level overview of the different areas within the system.
    Each image frame passes through this entire process one at a time, so that previous foot detections can be used to inform later ones.
}
\label{fig:high_level_system_overview}
\end{figure}

