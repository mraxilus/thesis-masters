\chapter{Background}
\label{chap:background}
\section{Object Tracking}
In today's society video capture devices are becoming more and more ubiquitous.  
Alongside this growth, the field of computer vision has made advances towards the task of tracking objects within recorded video sequences.
In 2006, Yilmaz et al.\ published a survey paper describing the available object tracking approaches~\cite{yilmaz2006object}.
They describe the general process of object tracking at a high level, and examine the differences between existing techniques.
Also,  they provide a clear definition of what object tracking is, and they outline three separate issues that that object trackers need to consider. 

According to Yilmaz et al.\ the aim of an object tracker is to generate the trajectory of an object over time within a given video sequence.
Trackers can achieve this by locating the object in every frame within the sequence.
Depending on the constraints, object trackers may have to deal with issues such as reflection, but there are more fundamental concerns. 
There are three main object tracking issues outlined in the paper. 
The first is finding a suitable method of representing the object. 
The second issue is deciding which features from the image frames will be used as input to the tracker. 
Finally, there is selecting the method to detect the object in the first place.


\subsection{Object Representation}
The tracking system must represent the object of interest via some mechanism.
There are a multitude of ways in which an object in a sequence can be represented, yet we will only consider a few common and related ones as they were described in the paper by Yilmaz et al.
Objects can be represented by their shape in the following ways:

\begin{itemize}
	\item{%
		An object can be represented by \textbf{points}, that is the center point can be marked as the location of the object. 
		Points are often used when the object of interest is particularly small relative to the image.
	}
	\item{%
		Another method is the use of \textbf{primitive geometric shapes}, such as rectangles and ellipses, to outline the region in which the object occupies. 
		This method is useful when tracking rigid objects, especially those which have simplistic shapes.
	}
	\item{%
		A third method would be to use the \textbf{silhouette or contour} of an object. 
		A contour would be the outline of the object of interest and the silhouette would be the area within the contour.
		This method is especially useful of non-rigid objects which have a complex or evolving structure.
	}
	\item{%
		Creating \textbf{articulated shape models} is an additional object representation technique which involves creating a kinematic model of the object.
		This technique could be used where the object of interest has several distinct parts connected by various joints, animals for example.
		Each of these distinct parts can be represented by ellipses or cylinders.
	}
	\item{%
		Additionally, \textbf{skeletal models} can be used.
		This involves the skeletonization of the silhouette of the object of interest, a method which can be used to represent both rigid and dynamic objects.
	}
\end{itemize}

Objects can also be represented by their appearance, however most of those methods are not particularly useful in the context of tracking humans.
When tracking animals, humans, or other objects with similar shapes, representing them by points or geometric shapes might prove inadequate.
The most promising options for object representation in the context of this project is likely to be silhouette/contour, articulated shape models, or skeletal models.
With that being said, according to the survey, when tracking humans, a silhouette or contour based representation is likely to be the best approach.


\subsection{Feature Selection}
Selecting an appropriate set of features is an important and necessary task for any model, and object tracking is not an exception.
When using video sequences as a data source, there are a variety of possible features which one can use.
In no order of importance or relevancy, the following are some available features which can be extracted from video footage for the purposes of tracking various objects over time:

\begin{itemize}
	\item{%
		One of the most commonly used features when dealing with pictures or video sequences is \textbf{color}.
		The color of an object is usually represented as greyscale or in some artificial color space.
		The chosen color space often tends to be \gls{rgb}, but can also be \gls{hsv} or some other variant.
	}
	\item{%
		The \textbf{edges} of objects can also be used and can be elicited from various algorithms such as the Canny Edge detector.
		The edge of an object within an image frame is found by identifying drastic changes in the image intensities usually in the \(x\) and \(y\) directions of the image.
	}
	\item{%
		\textbf{Optical flow} is where a field of displacement vectors represent the translation of each pixel within a region between frames in a sequence.
		One of the most common algorithms employed to extract the displacement vectors from a video sequence is the Lucas-Kanade method.
	}
	\item{%
		Finally, \textbf{texture} quantifies the smoothness and regularity of a surface using the intensity information provided from the image.
		This is similar to color with the caveat that a preprocessing step is needed to generate a texture descriptor for each identified surface.
		One such descriptor generating technique is a \gls{glcm}. 
	}
\end{itemize}

When selecting features it is important to note that they need not be used in isolation, as in fact, the various features are often used in combination with each other.
Although color is the most commonly used feature, it tends to be sensitive to variances in illumination, edges and textures are less so.
Yilmaz et al.\ outline optical flow as the method most used in object segmentation and tracking applications, so this will likely be a good feature for use when tracking human feet.
Edges may also be important as human silhouettes tend to produce strong edges in most scenarios, which may prove useful.


\subsection{Object Detection}
Every object tracker needs some method of detecting objects of interest.
Objects can be detected in either the first frame only or in every subsequent frame.
These methods can also opt to use the differences between neighbouring frames to utilise any temporal information that may exist.
Yilmaz et al.\ outline the following four object detection techniques:

\begin{itemize}
	\item{%
		One method involves the use of \textbf{point detectors}, which, using textures, finds uniquely identifiable interest points in the scene which are resilient to the effects of changes in the scene or perspective.
		An example of this method is the \gls{sift}.
	}
	\item{%
		Another method is to use \textbf{segmentation} to partition the image frames into regions which are as internally similar as possible.
		Ideally, the object of interest would be comprised of one or more partitions which could then be identified and tracked.
		Mean shift is an example of a clustering algorithm which can achieve this.
	}
	\item{%
		\textbf{Background subtraction} involves developing a model of the background in the scene, so that the foreground, i.e.\ moving objects, can be detected and separated, via subtraction, from the image frame.
		Many methods are available to perform background subtraction with various trade-offs and \gls{mog} is one such method.
	}
	\item{%
		Lastly, various \textbf{supervised classification} algorithms can be used to identify objects from examples provided during their training.
		Detection tends to involve a sliding widow approach, where the algorithm's feature detectors, developed from positive examples, attempt to match against thousands of possible regions in a presented image frame.
		One such supervised learning method is \gls{adaboost}.
	}
\end{itemize}

Out of all the available object detection methods background subtraction is the most commonly used when dealing with video sequences. 
Background subtraction also has the benefit of being able to deal with slight illumination variance and noise.
With that being said, according to Yilmaz et al.\ the downside to background subtraction is that it requires a stationary camera, but for our purposes that is already assumed to be the case.
Segmentation is another viable approach here, and may also be useful for separating the feet from a silhouette.


\subsection{Tracking Techniques}
Aside from the issues of object representation, feature selection, and object detection, it is important to understand object tracking as a whole, and to be aware of the higher-level strategies which can be employed.
Yilmaz et al.\ describe an object tracker as a system which generates the trajectory of an object of interest within a sequence by locating its position in every image frame.
They further divide this definition into two different approaches.
The first approach is where the object tracker detects the object in every frame in which it can, and then uses those detections to generate the locations of the object in frames with no detection. 
The second approach is to perform both the object detection and trajectory generation simultaneously, updating the object's location iteratively, and using only information obtained from the previous frames.
Three main high-level tracking approaches are presented by the paper, which are as follows:

\begin{itemize}
	\item{%
		\textbf{Point tracking} is an object tracking approach which represents objects as points, and requires the detection of these points to be performed in every image frame.
		Between consecutive image frames each point can have a position and motion vector.
	}
	\item{%
		Alternatively, \textbf{kernel tracking} refers to methods which tracks objects by their shape and appearance.
		Objects are tracked between frames by identifying changes in the objects motion between frames by modelling possible affine transformations the object could have undertaken.
	}
	\item{%
		\textbf{Silhouette tracking} is performed by using information obtainable from within the object of interest's occupied region, or silhouette, to track the object in each frame.
		This information is usually in the form of the silhouette's appearance or shape.
	}
\end{itemize}

Out of the three approaches, silhouette tracking appears to be the most relevant to tracking human feet.
Yilmaz et al.\ tout one of its advantages as being able to track objects with complex shapes, like the human head, shoulders, and hands.
It stands to reason the feet would also fall under this category.
Futhermore, silhouette tracking can be further divided into two sub-categories, contour evolution and shape matching.

Contour evolution involves evolving the object's initial contour to match the object that of the current image frame using various methods such as state space models.
Shape matching on the other hand pertains to matching a model of the object's shape to the located silhouette in the current image frame.
Shape matching is performed using techniques such as the Hough transform.
Additionally, both of these methods are likely to provide adequate results, so some experimentation would be needed.
\autoref{fig:taxonomy_of_object_tracking} provides a graphical overview on how each of these approaches relate to one another.

\begin{figure}[H]
\centering
\begin{tikzpicture}[node distance=2cm]
	\node (object) [box] {Object Tracking};
		\node (point) [box, below of=object, xshift=-3.5cm] {Point Tracking};
		\node (silhouette) [box, right of=point, xshift=1.5cm] {Silhouette Tracking};
			\node (contour) [box, below of=silhouette, xshift=-1.75cm] {Contour Evolution};
			\node (shape) [box, right of=contour, xshift=1.5cm] {Shape Matching};
		\node (kernel) [box, right of=silhouette, xshift=1.5cm] {Kernel Tracking};

	\draw [arrow] (object) -| (point);
	\draw [arrow] (object) -| (kernel);
	\draw [arrowThick] (object) -- (silhouette);
		\draw [arrowThick] (silhouette) -| (contour);
		\draw [arrowThick] (silhouette) -| (shape);
\end{tikzpicture}
\caption[Taxonomy of Object Tracking]{%
	A subset of the taxonomy of object tracking as presented in the paper by Yilmaz et al.~\cite{yilmaz2006object}.
	The thicker lines show the available options for the most promising techniques for tracking human feet.
}
\label{fig:taxonomy_of_object_tracking}
\end{figure}



\section{Background Subtraction}
Due to silhouette tracking being the most promising tracking approach for this project, we need to explore background subtraction in more depth.
In 2004, Piccardi published a survey pertaining to the accuracy and performance of various background subtraction algorithms~\cite{piccardi2004background}.
Piccardi defines background subtraction as a technique used to detect moving objects within a video sequence retrieved from a stationary camera.

The idea is to develop a model, or reference image, of the background scene. 
Differencing the current image frame with the background model will highlight any moving objects in the foreground.
The background model can also be kept up to date temporally, which avoids false positives due to changes in lighting or minor movement of the entire scene.

Piccardi analysed the performance of seven different background subtraction algorithms.
In the paper, the workings behind each algorithm is described, and each algorithm was benchmarked by speed, memory requirements and accuracy.
Each algorithm is also ordered by their level of subjective complexity, from least to most complex, which goes as follows:

\begin{enumerate}
	\item{Running Gaussian average.}
	\item{Temporal median filter.}
	\item{\gls{mog}.}
	\item{\gls{kde}.}
	\item{\gls{skda}.}
	\item{Co-occurrence of image variations.}
	\item{Eigenbackgrounds.}
\end{enumerate}

Taking all performance considerations into account, the running Gaussian average and the temporal median filter have good speed, accuracy, they utilise very little memory.
The \gls{mog} and \gls{kde} algorithms have very good model accuracy, but \gls{kde} has high memory usage which is in the order of one hundred image frames. 
The \gls{skda} algorithm attempts to approximate the \gls{kde} algorithm, with the benefit of running faster and using less memory.
Co-occurrence of image variations and eigenbackgrounds address additional issues with background subtraction, however they are more complex and offer good accuracy, and reasonable speed and memory usage.

Basically, when considering a background subtraction method to use there are quite a few different options with various trade-offs in terms of complexity, speed, accuracy, and memory usage.
For the purposes of tracking the feet of humans, experimentation will need to be performed in order to determine which method will be sufficient.
This project deals with pre-recorded video footage so real-time processing need not be a stringent requirement, so there is likely to be some flexibility here.
Ultimately, it would be best to choose the method which is both the most simplistic and easily accessible from the context of using specific image processing libraries such as OpenCV~\cite{bradski2000opencv}.



\section{Human Motion Analysis}
So far we have been mostly focussing on object tracking in general, but this project has a more specific focus.
This project is concerned with tracking both humans and subsequently their feet.
Now that we have an understanding of object tracking as a whole we can now focus on understanding the various approaches to human motion analysis.
To this end, Aggarwal et al.\ created a review of human motion analysis and listed three distinct areas wherein human motion is analysed~\cite{aggarwal1997human}.
The three areas are listed as follows:

\begin{itemize}
	\item{%
		The first type of approach involves analysis of the human \textbf{body structure}.
		This tends to include 2D or 3D analysis where the human body is represented as a stick figure, 2D contour, or a volumetric model.
		In this area, the motion of the human body is usually classified by the angular and linear velocities of various body parts.
	}
	\item{%
		The next approach involves \textbf{tracking} human motion without using body parts.
		This is a less complex approach when compared to body structure, as it focuses on tracking the motion of the human body as a whole, rather than its constituent parts.
		This tends to be from a camera system with either a singular view or multiple.
	}
	\item{%
		Another area is human activity \textbf{recognition}.
		This involves attempting to recognise human activities or movements from video footage.
		Human activity recognition tends to involve elastic movement of the face and articulated movement of body parts.
	}
\end{itemize}

This project is concerned solely with tracking and is unconcerned with recognising what particular activity is being enacted as it is assumed to be walking.
It may not be necessary to construct a joint based human model to track the positions of human feet, however it may prove to be necessary.
Due to this, the project may in fact involve the analysis of human body structure.
A visual overview of the different approaches available for human motion analysis can be seen in \autoref{fig:taxonomy_of_human_motion_analysis}.
The most likely scenario is that this project only involves the area of tracking, as previously mentioned tracking techniques should be sufficient.

\begin{figure}[H]
\centering
\begin{tikzpicture}[node distance=2cm]
	\node (human) [boxLarge] {Human Motion Analysis};
		\node (body) [boxMedium, below of=human, xshift=-3.8cm] {Body Structure};
		\node (tracking) [box, right of=body, xshift=1.8cm] {Tracking};
		\node (recognition) [box, right of=tracking, xshift=1.5cm] {Recognition};

	\draw [arrowThick] (human) -| (body);
	\draw [arrowThick] (human) -- (tracking);
	\draw [arrow] (human) -| (recognition);
\end{tikzpicture}
\caption[Taxonomy of Human Motion Analysis]{%
	A subset of the taxonomy of human motion analysis as presented in the paper from Aggarwal et al~\cite{aggarwal1997human}.
	The thicker lines represent the areas most related to this project, which is guaranteed to involve tracking and could potentially involve body structure analysis.
}
\label{fig:taxonomy_of_human_motion_analysis}
\end{figure}


\subsection{Tracking Human Body Parts}
Now that we've established which approaches and techniques are most promising for tracking human body parts, we can examine existing work which focuses on this specific application of object tracking.
In 2005, Jean et al.\ published a paper wherein which they propose a method of tracking human body parts for the purposes of gait analysis~\cite{jean2005body}.
They base their approach on a five points model of the human body which involves the head, hands, and feet.
We are mostly concerned with their approach towards feet tracking.

In their approach, Jean et al.\ first use background subtraction to identify the silhouette of the human figure.
They then assigned the lower thirty-three percent of the silhouette's height as the leg region to be used as the region for the detection of the figure's feet.
At this point, the leg region is passed to a custom algorithm which attempts to separate the two legs using a \gls{fbf} technique, in which they attempt to find the longest path which separates the two legs.
If the algorithm fails to separate the legs, then optical flow is used to attempt correspondence.

Once a valid separation path has been located by their algorithm, Jean et al.\ proceed to locate the foot positions.
They do so by aggregating the lower twenty-five percent of pixels in each identified leg region.
From the aggregated pixels they then use the center of mass of the pixels for each leg to label the positions of the feet.
Jean et al.\ proceeded to compare the results of their work to that of previous research which used a skeletal model, and noted significant improvement with their feet tracking.

Overall the method proposed by Jean et al.\ seems extremely promising, however their research was done using a side camera perspective.
This project involves video sequences where more of a frontal perspective is used as shown by \autoref{fig:camera_perspective_of_footage}.
To this end perhaps this solution may prove to be inadequate, or it may need to be combined with another human tracking approach.

\begin{figure}[H]
\centering
\includegraphics[width=0.8\textwidth]{camera_perspective}
\caption[Camera Perspective of Footage]{%
  An example image from the recordings obtained by Doughty et al.~\cite{doughty2015automatic}.
  In this you can observe the perspective this project will focus on, as well as the floor patterns which could negatively affect some algorithms.
}
\label{fig:camera_perspective_of_footage}
\end{figure}


\subsection{Dealing with Occlusion}
As an alternative to the aforementioned approach, Park et al.\ developed a human tracking system which is capable of segmenting and tracking multiple humans even in the presence of mutual occlusion~\cite{park2002segmentation}.
Park et al.\ used a blob generating technique which evolves blobs from pixels within the silhouette.
The silhouette is identified using background subtraction.
A model of the human body is applied to the blobs to identify individual human entities.

Each identified human is given an id and is tracked between consecutive frames over time.
This is accomplished by performing correspondence on the blob segments within each identified human entity.
Each blob is also associated with a specific body part.
To accomplish all of this, Park et al.\ utilise a \gls{gmm}, and a \gls{mrf} framework.
This is a more general and complex approach to human tracking however it is an alternative avenue which provides additional choice.



\subsection{Tracking by Detection}
If real-time processing speeds becomes a significant concern, then a supervised learning method may be more appropriate.
In 2011, Babenko et al.\ showed that using a technique called \gls{mil}, they can track objects in a video sequence when given its location in the first frame only~\cite{babenko2011robust}.
They demonstrate how other approaches, in this scenario, fall prone to drift due to slight inaccuracies in the tracker, and explain how \gls{mil} avoids falling prey to the same problems.

Although this approach seems to have merit, Babenko et al.\ acknowledge that this method is not entirely suited towards articulated objects, and they may require a parts-based approach instead.
Either way, this paper provides an approach to object tracking which significantly differs from the previous two tracking approaches.
Perhaps \gls{mil} could potentially be combined with the aforementioned approaches, or at the very least, there are some learnings which could be elicited from the research done by Babenko et al.



\section{Human Motion Perception}
Going back to the motivations of this project, human motion perception is a key driver in this research.
In 2015, Leonards et al.\ set out to determine if visual stimuli could affect the walking trajectory of human individuals~\cite{leonards2015treacherous}.
They note that current research in human locomotion research shows that when humans navigate natural environments they rely significantly on visual information.
They go on to prove that although man-made obstacle environment may seem exempt from this conclusion, they are not.

Leonards et al.\ demonstrate that oblique floor patterns presented to the lower visual field of human pedestrians caused substantial drifting away from the intended direction of travel.
They tested this by setting up a lab with common man-made floor patterns projected onto the floor, for example, the pattern shown in \autoref{fig:floor_pattern_example}.
They then investigated the ability for pedestrians to walk straight ahead for different orientations of the patterns.
Their findings show that a slab tile pattern oriented at certain angles caused a drift in line with that predicted by previous research on human motion perception.
Leonards et al.\ attribute this phenomenon to the aperture problem.
They describe the aperture problem as the inherent ambiguity of motion direction that arises from a line being seen through an aperture of a limited size.
In their case, the aperture is assumed to be the human lower visual field.

\begin{figure}[H]
\centering
\includegraphics[width=0.8\textwidth]{floor_pattern_example}
\caption[Example of a Slab Tile Floor Pattern]{%
	An example of a slab tile pattern adapted from one used by Leonards et al.~\cite{leonards2015treacherous}.
	Patterns were projected onto the floor of a dimly lit room from an overhead projection system.
}
\label{fig:floor_pattern_example}
\end{figure}

In their analysis, Leonards et al.\ note that the experiment has potential issues as it was performed in a controlled environment, with low lighting, a narrow corridor, and high contrast pattern projected onto the floor.
This is an unlikely scenario to be produced in a real world environment which they do admit.
Following on from this work, in an attempt to fill the gaps mentioned above, Doughty et al.\ proceeded to test the findings previously made by Leonards et al.\ in a real world environment~\cite{doughty2015automatic}.

Doughty et al.\ setup a GoPro camera to record human individuals walking through a corridor within the University of Bristol grounds.
They also used black tape to display patterns on the floor, similar to those used by Leonards et al.
The patterns were rotated over time so various angles and densities were recorded.
Ultimately, similar findings to those reported by Leonards et al.\ were discovered.
The issue, however, is that Doughty et al.\ tracked only the head and mapped that to the floor, which may be ignoring potentially vital gait information which could provide further insights into the observed effect.
This project aimed to use the footage collected by Doughty et al., as shown in \autoref{fig:camera_perspective_of_footage}, to evaluate and test the implemented feet tracker.

