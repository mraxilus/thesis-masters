\chapter{Introduction}
In 2015 Leonards et al.\ performed a controlled study investigating how visual input affects the walking trajectories of individuals. 
The team analysed how floor patterns, oriented at differing angles and frequencies, altered the trajectory of various gaits.
They found evidence that oblique angles in the patterns caused ``substantial drift away from the intended direction of travel'', which in turn was attributed to the aperture problem~\cite{leonards2015treacherous}.
Doughty et al.\ then proceeded to confirm their findings by setting up a real world version of the investigation~\cite{doughty2015automatic}.
This involved recording university personnel walking through a corridor with similar floor patterns taped on the floor.
These findings suggest that knowledge obtained from research in this area could potentially lead to fewer falls in the older population, and fewer accidents due to people veering off from their intended direction of travel.



\section{Aims and Objectives}
In~\cite{leonards2015treacherous}, calculating the trajectory of silhouettes involved data obtained from a tracker, placed on the sternum.
In~\cite{doughty2015automatic}, they tracked the head of a silhouette and then projected onto the floor.
The problem with these two methods is that they only track alterations in trajectory.
This could be ignoring information which may be important when analysing changes in gait cycles.
Some examples of the potentially useful information would be step and stride length, walking base, and cadence.
This project aims to investigate whether, using current methods in computer vision, we are able to track the feet positions of silhouettes over time using remote camera footage.
To achieve this, this project set out to accomplish the following objectives:

\begin{enumerate}
	\item Investigate methods for identifying silhouettes, and subsequently the positions of their feet.
	\item Locate approaches which handle object tracking whilst taking into account the presence of self-occlusion.
	\item Develop the ground truth of feet locations from the existing recordings obtained by Doughty et al.
	\item Implement and possibly combine methods, comparing model results with the established ground truth.
	\item Compare findings made with previous background research, and noting any interesting insights discovered.
\end{enumerate}



\section{Project Scope}
This project aimed to investigate whether, using current methods in computer vision, we can track the feet positions of human silhouettes.
It locates existing methods for silhouette identification and object tracking in the presence of reflection and luminance issues.

The initial research focus of this project was to identify a robust method which extracts silhouettes from video footage.
Background subtraction is one of the most commonly used techniques when tracking objects from a stationary camera~\cite{piccardi2004background}.
To perform background subtraction, we need to make a choice between the existing approaches (running Gaussian average, temporal median filter, Gaussian Mixture Model, Kernel Density Estimation, etc.).

This project's primary research focus is on object tracking from a single remote camera.
In this specific context, this involves tracking the feet positions of silhouettes, rather than using other object tracking methods (point tracking, kernel tracking)~\cite{yilmaz2006object}.
When tracking individual foot positions, the method used must be resilient to the presence of reflections.
Thus, this research includes investigating which particular method of silhouette tracking is more applicable for our purposes (contour evolution, matching shapes).



\section{Added Value}
The resulting output of this project is the investigation and implementation of a method by which to track the feet positions of silhouettes using recorded footage.
This project augments previous work performed in this area by allowing for the retainment of potentially useful gait cycle information.
Abstracting trajectory paths from the information obtained during tracking would also allow for comparison with the results of the existing research.
Ultimately, this project shows that it is possible to track feet, using current computer vision techniques, to such a degree that it can provide a foundation for extracting potentially useful information.

Due to the variety of potential techniques available, this project involved investigation into which of these are more applicable to our intended use case. 
In order to evaluate and validate the identified techniques, a significant amount of software development was undertaken, using various computer vision and machine learning tools.
The aforementioned requirements indicate that this project is equally a combination of both a Type II (investigation) and Type I (software development) project.



\section{Overview and Structure}
Excluding the front matter, introduction, conclusion, and end matter, this dissertation includes four main chapters.
In the order in which they appear in the text, each of these chapters are briefly summarized below:

\begin{description}
	\item[Background,]{%
        \autoref{chap:background}, is the literature section of this investigation, organised in a general to specific fashion.
        Firstly, it presents an overview of existing object tracking methods.
        This provides the context for an explanation of background subtraction, and why this project uses it.
        Additionally,  this chapter explores the process of analysing human motion to show why feet tracking is an important endeavour.
        Finally, it covers motion perception effects, demonstrating why there exist interest in the challenge of tracking the human gait.
	}
	\item[Implementation,]{%
        \autoref{chap:implementation}, covers the intricacies of the developed system.
        It first describes the source dataset retrieved from Doughty et al., and then goes on to cover the different steps required to produce usable input sequences, for use by our model.
        Afterwards, this chapter describes the techniques used to combine various background subtraction methods, which allowed for the removal of floor reflections in some video sequences.
        Lastly, the heuristic approaches used to accomplish feet tracking are thoroughly outlined and explained.
	}
	\item[Results,]{%
        \autoref{chap:results}, provides scoring metrics retrieved from comparing the systems detections with the labelled data.
        The process of creating the labelled data, or ground truth, is illustrated in the first part of this chapter.
        Following this, explanations of the different scoring metrics and their differences are provided.
        Subsequently, this chapter lays out the results retrieved from applying the scoring metrics on the detections made for each example sequence.
	}
	\item[Evaluation,]{%
        \autoref{chap:evaluation}, evaluates the project as a whole, comparing the aims and objectives with the actual resulting implementation.
		This chapter also discusses the encountered challenges, and explores the various options that were available.
		Additionally, outlined in the evaluation is the overall contributions that this project has made.
	}
\end{description}

