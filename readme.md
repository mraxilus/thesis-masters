[Masters Thesis][linkedin]
================================================
![GitHub version][version_badge]

_Undertaking a Masters thesis from the University of Bristol's MSc Advanced Computing degree._

License
-------
Copyright © Mr Axilus.
This project is licensed under [CC BY-NC-SA 4.0][license].

[license]: https://creativecommons.org/licenses/by-nc-sa/4.0/
[linkedin]: https://www.linkedin.com/in/mraxilus
[version_badge]: https://badge.fury.io/gh/mraxilus-uob%2Fmasters-thesis.svg 
